using CardExchangerServer.Data;
using CardExchangerServer.Interfaces.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Threading.Tasks;

namespace CardExchangerServer
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                var context = services.GetRequiredService<CardExchangerContext>();
                var generator = new Generator(services.GetRequiredService<ICardsService>(),
                                              services.GetRequiredService<ISetsService>(),
                                              services.GetRequiredService<IUsersService>(),
                                              services.GetRequiredService<IDialoguesService>(),
                                              services.GetRequiredService<IExchangesService>());
                await generator.Initialize();
            }

            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
