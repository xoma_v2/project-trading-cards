﻿using CardExchangerServer.Data;
using CardExchangerServer.Data.Models;
using CardExchangerServer.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardExchangerServer.Repositories
{
    public class ExchangesRepository : RepositoryBase, IExchangesRepository
    {
        public ExchangesRepository(CardExchangerContext context) : base(context)
        { }

        public async Task<IEnumerable<Exchange>> GetDialogueExchangesAsync(long dialogueId)
        {
            return await _context.Exchanges
                .Where(e => e.DialogueId == dialogueId)
                .ToListAsync()
                .ConfigureAwait(false);
        }

        public async Task<IEnumerable<Exchange>> GetUserExchangesAsync(long userId)
        {
            return await _context.Exchanges
                .Where(e => e.User1Id == userId || e.User2Id == userId)
                .ToListAsync()
                .ConfigureAwait(false);
        }

        public async Task<Exchange> GetExchangeByIdAsync(long id)
        {
            return await _context.Exchanges
                .AsNoTracking()
                .Where(e => e.Id == id)
                .SingleOrDefaultAsync()
                .ConfigureAwait(false);
        }

        public async Task AddExchangeAsync(Exchange exchange)
        {
            await _context.Exchanges.AddAsync(exchange).ConfigureAwait(false);
        }

        public void UpdateExchange(Exchange exchange)
        {
            _context.Exchanges.Update(exchange);
        }

        public void RemoveExchange(Exchange exchange)
        {
            _context.Exchanges.Remove(exchange);
        }

        public async Task<IEnumerable<ExchangeCard>> GetExchangeCardsAsync(long exchangeId)
        {
            return await _context.ExchangeCards
                .Where(ec => ec.ExchangeId == exchangeId)
                .ToListAsync()
                .ConfigureAwait(false);
        }

        public async Task<IEnumerable<ExchangeCard>> GetUserExchangeCardsAsync(long exchangeId, long userId)
        {
            return await _context.ExchangeCards
                .Where(ec => ec.ExchangeId == exchangeId && ec.UserId == userId)
                .ToListAsync()
                .ConfigureAwait(false);
        }

        public async Task<ExchangeCard> GetExchangeCardByValueAsync(ExchangeCard exchangeCard)
        {
            return await _context.ExchangeCards
                .AsNoTracking()
                .Where(ec => ec.ExchangeId == exchangeCard.ExchangeId &&
                       ec.UserId == exchangeCard.UserId &&
                       ec.CardId == exchangeCard.CardId)
                .SingleOrDefaultAsync()
                .ConfigureAwait(false);
        }

        public async Task AddExchangeCardAsync(ExchangeCard exchangeCard)
        {
            await _context.ExchangeCards.AddAsync(exchangeCard).ConfigureAwait(false);
        }

        public void UpdateExchangeCard(ExchangeCard exchangeCard)
        {
            _context.ExchangeCards.Update(exchangeCard);
        }

        public void RemoveExchangeCard(ExchangeCard exchangeCard)
        {
            _context.ExchangeCards.Remove(exchangeCard);
        }
    }
}
