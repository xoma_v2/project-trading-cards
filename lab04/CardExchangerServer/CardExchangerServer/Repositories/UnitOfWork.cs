﻿using CardExchangerServer.Data;
using CardExchangerServer.Interfaces;
using System.Threading.Tasks;

namespace CardExchangerServer.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly CardExchangerContext _context;

        public UnitOfWork(CardExchangerContext context)
        {
            _context = context;
        }

        public async Task CompleteAsync()
        {
            await _context.SaveChangesAsync().ConfigureAwait(false);
        }
    }
}
