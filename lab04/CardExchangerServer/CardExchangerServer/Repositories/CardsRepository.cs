﻿using CardExchangerServer.Data;
using CardExchangerServer.Data.Models;
using CardExchangerServer.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardExchangerServer.Repositories
{
    public class CardsRepository : RepositoryBase, ICardsRepository
    {
        public CardsRepository(CardExchangerContext context) : base(context)
        { }

        public async Task<IEnumerable<Card>> CardListAsync()
        {
            return await _context.Cards.ToListAsync().ConfigureAwait(false);
        }

        public async Task<IEnumerable<Card>> SetCardListAsync(long id)
        {
            return await _context.Cards
                .Where(c => c.SetId == id)
                .ToListAsync()
                .ConfigureAwait(false);
        }

        public async Task<IEnumerable<Card>> FindByTextAsync(string cardText)
        {
            return await _context.Cards
                .Where(c => EF.Functions.Like(c.Name.ToLower(), $"%{cardText.ToLower()}%") ||
                            EF.Functions.Like(c.Description.ToLower(), $"%{cardText.ToLower()}%"))
                .ToListAsync()
                .ConfigureAwait(false);
        }

        public async Task<IEnumerable<Card>> FindBySetCodeAsync(string setCode)
        {
            return await _context.Cards
                .Where(c => EF.Functions.Like(c.SetCode.ToLower(), $"%{setCode.ToLower()}%"))
                .ToListAsync()
                .ConfigureAwait(false);
        }

        public async Task<IEnumerable<Card>> FindByCardTypeAsync(string cardType)
        {
            cardType = cardType.ToLower();
            return (await _context.Cards
                .ToListAsync()
                .ConfigureAwait(false))
                .Where(c => c.Types.Where(t => t.ToLower().Contains(cardType)).Any());
        }

        public async Task<Card> GetByIdAsync(long id)
        {
            return await _context.Cards
                .AsNoTracking()
                .Where(c => c.Id == id)
                .SingleOrDefaultAsync()
                .ConfigureAwait(false);
        }

        public async Task AddAsync(Card card)
        {
            await _context.Cards.AddAsync(card);
        }

        public void Update(Card card)
        {
            _context.Cards.Update(card);
        }

        public void Remove(Card card)
        {
            _context.Cards.Remove(card);
        }

        public async Task<IEnumerable<Card>> UserCardListAsync(long id)
        {
            return await (from collection in _context.Collections
                          where collection.UserId == id
                          join card in _context.Cards on collection.CardId equals card.Id
                          select card).ToListAsync().ConfigureAwait(false);
        }

        public async Task<IEnumerable<Collection>> CollectionAsync(long id)
        {
            return await _context.Collections
                .Where(c => c.UserId == id)
                .ToListAsync()
                .ConfigureAwait(false);
        }

        public async Task AddCardToCollectionAsync(Collection collection)
        {
            await _context.Collections.AddAsync(collection).ConfigureAwait(false);
        }

        public void RemoveCardFromCollection(Collection collection)
        {
            _context.Collections.Remove(collection);
        }

        public async Task<Collection> GetCollectionByIdAsync(long userId, long cardId)
        {
            return await _context.Collections
                .Where(c => c.UserId == userId && c.CardId == cardId)
                .SingleOrDefaultAsync()
                .ConfigureAwait(false);
        }

        public void UpdateCollection(Collection collection)
        {
            _context.Collections.Update(collection);
        }
    }
}
