﻿using CardExchangerServer.Data;
using CardExchangerServer.Data.Models;
using CardExchangerServer.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardExchangerServer.Repositories
{
    public class SetsRepository : RepositoryBase, ISetsRepository
    {
        public SetsRepository(CardExchangerContext context) : base(context)
        {

        }

        public async Task<IEnumerable<Set>> ListAsync()
        {
            return await _context.Sets.ToListAsync().ConfigureAwait(false);
        }

        public async Task<IEnumerable<Set>> FindByNameAsync(string name)
        {
            return await _context.Sets
                .Where(s => EF.Functions.Like(s.Name.ToLower(), $"%{name.ToLower()}%"))
                .ToListAsync()
                .ConfigureAwait(false);
        }

        public async Task<IEnumerable<Set>> FindByCodeAsync(string code)
        {
            return await _context.Sets
                .Where(s => EF.Functions.Like(s.Code.ToLower(), $"%{code.ToLower()}%"))
                .ToListAsync()
                .ConfigureAwait(false);
        }

        public async Task<Set> GetByIdAsync(long id)
        {
            return await _context.Sets
                .AsNoTracking()
                .Where(s => s.Id == id)
                .SingleOrDefaultAsync()
                .ConfigureAwait(false);
        }

        public async Task AddAsync(Set set)
        {
            await _context.Sets.AddAsync(set);
        }

        public void Update(Set set)
        {
            _context.Sets.Update(set);
        }

        public void Remove(Set set)
        {
            _context.Sets.Remove(set);
        }
    }
}
