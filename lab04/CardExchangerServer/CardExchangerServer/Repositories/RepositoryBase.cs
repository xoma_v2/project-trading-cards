﻿using CardExchangerServer.Data;

namespace CardExchangerServer.Repositories
{
    public abstract class RepositoryBase
    {
        protected readonly CardExchangerContext _context;

        public RepositoryBase(CardExchangerContext context)
        {
            _context = context;
        }
    }
}
