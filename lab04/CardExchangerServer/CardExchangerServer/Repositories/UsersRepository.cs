﻿using CardExchangerServer.Data;
using CardExchangerServer.Data.Models;
using CardExchangerServer.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardExchangerServer.Repositories
{
    public class UsersRepository : RepositoryBase, IUsersRepository
    {
        public UsersRepository(CardExchangerContext context) : base(context)
        {
        }

        public async Task<IEnumerable<User>> ListAsync()
        {
            return await _context.Users.ToListAsync().ConfigureAwait(false);
        }

        public async Task<IEnumerable<User>> FindByLoginAsync(string login)
        {
            return await _context.Users
                .Where(u => EF.Functions.Like(u.Login.ToLower(), $"%{login.ToLower()}%"))
                .ToListAsync()
                .ConfigureAwait(false);
        }

        public async Task<User> GetByIdAsync(long id)
        {
            return await _context.Users
                .AsNoTracking()
                .Where(u => u.Id == id)
                .SingleOrDefaultAsync()
                .ConfigureAwait(false);
        }

        public async Task<User> GetByLoginAsync(string login)
        {
            return await _context.Users
                .AsNoTracking()
                .Where(u => u.Login == login)
                .SingleOrDefaultAsync()
                .ConfigureAwait(false);
        }

        public void Remove(User user)
        {
            _context.Remove(user);
        }

        public async Task<User> AddAsync(User user)
        {
            await _context.Users.AddAsync(user).ConfigureAwait(true);

            return user;
        }

        public User Update(User user)
        {
            _context.Users.Update(user);

            return user;
        }
    }
}
