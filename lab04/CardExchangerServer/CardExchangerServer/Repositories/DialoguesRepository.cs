﻿using CardExchangerServer.Data;
using CardExchangerServer.Data.Models;
using CardExchangerServer.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardExchangerServer.Repositories
{
    public class DialoguesRepository : RepositoryBase, IDialoguesRepository
    {
        public DialoguesRepository(CardExchangerContext context) : base(context)
        { }

        public async Task<IEnumerable<Dialogue>> GetUserDialoguesAsync(long userId)
        {
            return await _context.Dialogues
                .Where(d => d.User1Id == userId || d.User2Id == userId)
                .ToListAsync()
                .ConfigureAwait(false);
        }

        public async Task<Dialogue> GetDialogueByIdAsync(long id)
        {
            return await _context.Dialogues
                .AsNoTracking()
                .Where(d => d.Id == id)
                .SingleOrDefaultAsync()
                .ConfigureAwait(false);
        }

        public async Task<Dialogue> GetDialogueByUsersIdAsync(long user1Id, long user2Id)
        {
            return await _context.Dialogues
                .AsNoTracking()
                .Where(d => (d.User1Id == user1Id && d.User2Id == user2Id) || 
                            (d.User2Id == user1Id && d.User1Id == user2Id))
                .SingleOrDefaultAsync()
                .ConfigureAwait(false);
        }

        public async Task AddDialogueAsync(Dialogue dialogue)
        {
            await _context.Dialogues.AddAsync(dialogue).ConfigureAwait(false);
        }

        public void RemoveDialogue(Dialogue dialogue)
        {
            _context.Dialogues.Remove(dialogue);
        }

        public async Task<IEnumerable<Message>> GetDialogueMessagesAsync(long dialogueId)
        {
            return await _context.Messages
                .Where(m => m.DialogueId == dialogueId)
                .ToListAsync()
                .ConfigureAwait(false);
        }

        public async Task<IEnumerable<Message>> GetUserMessagesAsync(long dialogueId, long userId)
        {
            return await _context.Messages
                .Where(m => m.DialogueId == dialogueId && m.UserId == userId)
                .ToListAsync()
                .ConfigureAwait(false);
        }

        public async Task<Message> GetMessageByIdAsync(long id)
        {
            return await _context.Messages
                .AsNoTracking()
                .Where(m => m.Id == id)
                .SingleOrDefaultAsync()
                .ConfigureAwait(false);
        }

        public async Task AddMessageAsync(Message message)
        {
            await _context.Messages.AddAsync(message).ConfigureAwait(false);
        }

        public void UpdateMessage(Message message)
        {
            _context.Messages.Update(message);
        }

        public void RemoveMessage(Message message)
        {
            _context.Messages.Remove(message);
        }
    }
}
