﻿using CardExchangerServer.Data.Resources;
using CardExchangerServer.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CardExchangerServer.Hubs
{
    [Authorize(Roles ="User, Admin")]
    public class ChatHub : Hub
    {
        private readonly IDialoguesService _dialoguesService;

        public ChatHub(IDialoguesService dialoguesService)
        {
            _dialoguesService = dialoguesService;
        }

        public override async Task OnConnectedAsync()
        {
            var response = await _dialoguesService
                .GetUserDialoguesAsync(long.Parse(Context.User
                .FindFirst(x => x.Type == ClaimTypes.NameIdentifier).Value))
                .ConfigureAwait(true);
            if (!response.Success)
            {
                await Clients.Caller.SendAsync("onConnectError", response.Message);
            }

            foreach (var dialogue in response.Object)
            {
                await Groups.AddToGroupAsync(Context.ConnectionId, dialogue.Id.ToString());
            }
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            var response = await _dialoguesService
                .GetUserDialoguesAsync(long.Parse(Context.User
                .FindFirst(x => x.Type == ClaimTypes.NameIdentifier).Value))
                .ConfigureAwait(true);
            if (!response.Success)
            {
                await Clients.Caller.SendAsync("onConnectError", response.Message);
            }

            foreach (var dialogue in response.Object)
            {
                await Groups.RemoveFromGroupAsync(Context.ConnectionId, dialogue.Id.ToString());
            }
        }

        /*
        public async Task JoinGroup(string groupName)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, groupName);
        }

        public async Task LeaveGroup(string groupName)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, groupName);
        }
        */

        public async Task Send(MessageResource message)
        {
            var response = await _dialoguesService
                .AddMessageAsync(message.DialogueId, message)
                .ConfigureAwait(true);
            if (!response.Success)
            {
                await Clients.Caller.SendAsync("onSendError", response.Message);
            }

            await Clients
                .Group(response.Object.DialogueId.ToString())
                .SendAsync("Recieve", response.Object);
        }
    }
}
