﻿namespace CardExchangerServer.Communication
{
    public class ObjectResponse<T> : ResponseBase
    {
        public T Object { get; private set; }
        public int? Count { get; set; }

        private ObjectResponse(bool success, string message, T obj, int statusCode)
            : base(success, message, statusCode)
        {
            Object = obj;
        }

        public ObjectResponse(T obj) : this(true, string.Empty, obj, 200)
        { }

        public ObjectResponse(string message, int statusCode)
            : this(false, message, default(T), statusCode)
        { }
    }
}
