﻿namespace CardExchangerServer.Communication
{
    public class EmptyResponse : ResponseBase
    {
        private EmptyResponse(bool success, string message, int statusCode)
            : base(success, message, statusCode)
        { }

        public EmptyResponse() : this(true, string.Empty, 200)
        { }

        public EmptyResponse(string message, int statusCode)
            : this(false, message, statusCode)
        { }
    }
}
