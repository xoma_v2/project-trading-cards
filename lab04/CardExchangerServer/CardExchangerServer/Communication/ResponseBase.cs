﻿namespace CardExchangerServer.Communication
{
    public abstract class ResponseBase
    {
        public bool Success { get; protected set; }
        public string Message { get; protected set; }
        public int StatusCode { get; protected set; }

        public ResponseBase(bool success, string message, int statusCode)
        {
            Success = success;
            Message = message;
            StatusCode = statusCode;
        }
    }
}
