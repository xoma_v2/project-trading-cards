﻿using System.Threading.Tasks;

namespace CardExchangerServer.Interfaces
{
    public interface IUnitOfWork
    {
        Task CompleteAsync();
    }
}
