﻿using CardExchangerServer.Communication;
using CardExchangerServer.Data.Resources;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CardExchangerServer.Interfaces.Services
{
    public interface IExchangesService
    {
        Task<ObjectResponse<IEnumerable<ExchangeResource>>> GetDialogueExchangesAsync(long dialogueId);
        Task<ObjectResponse<IEnumerable<ExchangeResource>>> GetUserExchangesAsync(long userId);
        Task<ObjectResponse<ExchangeResource>> GetExchangeByIdAsync(long id);
        Task<ObjectResponse<ExchangeResource>> AddExchangeAsync(ExchangeResource resource);
        Task<ObjectResponse<ExchangeResource>> UpdateExchangeAsync(ExchangeResource resource);
        Task<EmptyResponse> DeleteExchangeAsync(long id);

        Task<ObjectResponse<IEnumerable<ExchangeCardResource>>> GetExchangeCardsAsync(long exchangeId);
        Task<ObjectResponse<IEnumerable<ExchangeCardResource>>> GetUserExchangeCardsAsync(long exchangeId, long userId);
        Task<ObjectResponse<ExchangeCardResource>> AddExchangeCardAsync(long exchangeId, ExchangeCardResource resource);
        Task<ObjectResponse<ExchangeCardResource>> UpdateExchangeCardAsync(long exchangeId, ExchangeCardResource resource);
        Task<EmptyResponse> DeleteExchangeCardAsync(long exchangeId, ExchangeCardResource resource);
    }
}
