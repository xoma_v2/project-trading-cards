﻿using CardExchangerServer.Communication;
using CardExchangerServer.Data.Resources;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CardExchangerServer.Interfaces.Services
{
    public interface IDialoguesService
    {
        Task<ObjectResponse<IEnumerable<DialogueResource>>> GetUserDialoguesAsync(long userId);
        Task<ObjectResponse<DialogueResource>> GetDialogueByIdAsync(long id);
        Task<ObjectResponse<DialogueResource>> AddDialogueAsync(DialogueResource resource);
        Task<EmptyResponse> DeleteDialogueAsync(long id);

        Task<ObjectResponse<IEnumerable<MessageResource>>> GetDialogueMessagesAsync(long dialogueId);
        Task<ObjectResponse<MessageResource>> GetLastDialogueMessageAsync(long dialogueId);
        Task<ObjectResponse<MessageResource>> AddMessageAsync(long dialogueId, MessageResource resource);
        Task<ObjectResponse<MessageResource>> UpdateMessageAsync(long dialogueId, MessageResource resource);
        Task<EmptyResponse> DeleteMessageAsync(long dialogueId, long messageId);
    }
}
