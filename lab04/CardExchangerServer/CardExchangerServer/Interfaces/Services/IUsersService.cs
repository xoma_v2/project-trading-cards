﻿using CardExchangerServer.Communication;
using CardExchangerServer.Data.Resources;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CardExchangerServer.Interfaces.Services
{
    public interface IUsersService
    {
        Task<ObjectResponse<OutUserResource>> LoginAsync(InUserResource resource);
        Task<ObjectResponse<OutUserResource>> SinginAsync(InUserResource resource);

        Task<ObjectResponse<IEnumerable<OutUserResource>>> GetUserListAsync(string? login);
        Task<ObjectResponse<OutUserResource>> GetUserByIdAsync(long id);
        Task<ObjectResponse<OutUserResource>> UpdateUserAsync(InUserResource resource);
        Task<EmptyResponse> DeleteUserByIdAsync(long id);

    }
}
