﻿using CardExchangerServer.Communication;
using CardExchangerServer.Data.Resources;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CardExchangerServer.Interfaces.Services
{
    public interface ISetsService
    {
        Task<ObjectResponse<IEnumerable<SetResource>>> GetSetListAsync();
        Task<ObjectResponse<IEnumerable<SetResource>>> FindSetsAsync(string? setName,
                                                                     string? setCode,
                                                                     int offset = 0,
                                                                     int limit = 0,
                                                                     bool onlyCount = false);
        Task<ObjectResponse<SetResource>> GetSetByIdAsync(long id);
        Task<ObjectResponse<SetResource>> AddSetAsync(SetResource resource);
        Task<ObjectResponse<SetResource>> UpdateSetAsync(SetResource resource);
        Task<EmptyResponse> DeleteSetByIdAsync(long id);
    }
}
