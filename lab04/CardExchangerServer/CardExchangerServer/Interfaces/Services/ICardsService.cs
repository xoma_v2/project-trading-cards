﻿using CardExchangerServer.Communication;
using CardExchangerServer.Data.Resources;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CardExchangerServer.Interfaces.Services
{
    public interface ICardsService
    {
        Task<ObjectResponse<IEnumerable<CardResource>>> GetCardListAsync();
        Task<ObjectResponse<IEnumerable<CardResource>>> GetSetCardListAsync(long id);
        Task<ObjectResponse<IEnumerable<CardResource>>> GetUserCardsAsync(long id,
                                                                          int offset = 0,
                                                                          int limit = 0);
        Task<ObjectResponse<IEnumerable<CardResource>>> FindCardsAsync(string? cardText,
                                                                       string? setCode,
                                                                       string? cardType,
                                                                       int offset = 0,
                                                                       int limit = 0,
                                                                       bool onlyCount = false);

        Task<ObjectResponse<CardResource>> GetCardByIdAsync(long id);
        Task<ObjectResponse<CardResource>> AddCardAsync(CardResource resource);
        Task<ObjectResponse<CardResource>> UpdateCardAsync(CardResource resource);
        Task<EmptyResponse> DeleteCardByIdAsync(long id);

        Task<ObjectResponse<CollectionResource>> AddUserCardAsync(long userId, CollectionResource resource);
        Task<EmptyResponse> DeleteUserCardAsync(long userId, CollectionResource resource);
    }
}
