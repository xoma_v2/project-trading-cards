﻿using CardExchangerServer.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CardExchangerServer.Interfaces.Repositories
{
    public interface IExchangesRepository
    {
        Task<IEnumerable<Exchange>> GetDialogueExchangesAsync(long dialogueId);
        Task<IEnumerable<Exchange>> GetUserExchangesAsync(long userId);
        Task<Exchange> GetExchangeByIdAsync(long id);
        Task AddExchangeAsync(Exchange exchange);
        void UpdateExchange(Exchange exchange);
        void RemoveExchange(Exchange exchange);

        Task<IEnumerable<ExchangeCard>> GetExchangeCardsAsync(long exchangeId);
        Task<IEnumerable<ExchangeCard>> GetUserExchangeCardsAsync(long exchangeId, long userId);
        Task<ExchangeCard> GetExchangeCardByValueAsync(ExchangeCard exchangeCard);
        Task AddExchangeCardAsync(ExchangeCard exchangeCard);
        void UpdateExchangeCard(ExchangeCard exchangeCard);
        void RemoveExchangeCard(ExchangeCard exchangeCard);
    }
}
