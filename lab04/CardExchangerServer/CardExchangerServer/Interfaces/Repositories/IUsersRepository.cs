﻿using CardExchangerServer.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CardExchangerServer.Interfaces.Repositories
{
    public interface IUsersRepository
    {
        Task<IEnumerable<User>> ListAsync();
        Task<IEnumerable<User>> FindByLoginAsync(string login);
        Task<User> GetByIdAsync(long id);
        Task<User> GetByLoginAsync(string login);
        Task<User> AddAsync(User user);
        void Remove(User user);
        User Update(User user);
    }
}
