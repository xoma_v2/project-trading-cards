﻿using CardExchangerServer.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CardExchangerServer.Interfaces.Repositories
{
    public interface ICardsRepository
    {
        Task<IEnumerable<Card>> CardListAsync();
        Task<IEnumerable<Card>> SetCardListAsync(long id);
        Task<IEnumerable<Card>> FindByTextAsync(string cardText);
        Task<IEnumerable<Card>> FindBySetCodeAsync(string setCode);
        Task<IEnumerable<Card>> FindByCardTypeAsync(string cardType);
        Task<Card> GetByIdAsync(long id);
        Task AddAsync(Card card);
        void Update(Card card);
        void Remove(Card card);

        Task<IEnumerable<Collection>> CollectionAsync(long id);
        Task<IEnumerable<Card>> UserCardListAsync(long id);
        Task<Collection> GetCollectionByIdAsync(long userId, long cardId);
        Task AddCardToCollectionAsync(Collection collection);
        void UpdateCollection(Collection collection);
        void RemoveCardFromCollection(Collection collection);
    }
}
