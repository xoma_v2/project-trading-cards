﻿using CardExchangerServer.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CardExchangerServer.Interfaces.Repositories
{
    public interface ISetsRepository
    {
        Task<IEnumerable<Set>> ListAsync();
        Task<IEnumerable<Set>> FindByNameAsync(string name);
        Task<IEnumerable<Set>> FindByCodeAsync(string code);
        Task<Set> GetByIdAsync(long id);
        Task AddAsync(Set set);
        void Update(Set set);
        void Remove(Set set);
    }
}
