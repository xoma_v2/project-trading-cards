﻿using CardExchangerServer.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CardExchangerServer.Interfaces.Repositories
{
    public interface IDialoguesRepository
    {
        Task<IEnumerable<Dialogue>> GetUserDialoguesAsync(long userId);
        Task<Dialogue> GetDialogueByIdAsync(long id);
        Task<Dialogue> GetDialogueByUsersIdAsync(long user1Id, long user2Id);
        Task AddDialogueAsync(Dialogue dialogue);
        void RemoveDialogue(Dialogue dialogue);

        Task<IEnumerable<Message>> GetDialogueMessagesAsync(long dialogueId);
        Task<IEnumerable<Message>> GetUserMessagesAsync(long dialogueId, long userId);
        Task<Message> GetMessageByIdAsync(long id);
        Task AddMessageAsync(Message message);
        void UpdateMessage(Message message);
        void RemoveMessage(Message message);
    }
}
