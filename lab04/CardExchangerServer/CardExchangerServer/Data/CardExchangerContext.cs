﻿using CardExchangerServer.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace CardExchangerServer.Data
{
    public class CardExchangerContext : DbContext
    {
        public DbSet<Card> Cards { get; set; }
        public DbSet<Collection> Collections { get; set; }
        public DbSet<Dialogue> Dialogues { get; set; }
        public DbSet<Exchange> Exchanges { get; set; }
        public DbSet<ExchangeCard> ExchangeCards { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Set> Sets { get; set; }
        public DbSet<User> Users { get; set; }

        public CardExchangerContext(DbContextOptions options) : base(options)
        {
            //Database.EnsureDeleted(); 
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CardConfiguration()); 
            modelBuilder.ApplyConfiguration(new CollectionConfiguration());
            modelBuilder.ApplyConfiguration(new DialogueConfiguration());
            modelBuilder.ApplyConfiguration(new ExchangeConfiguration());
            modelBuilder.ApplyConfiguration(new ExchangeCardConfiguration());
            modelBuilder.ApplyConfiguration(new MessageConfiguration());
            modelBuilder.ApplyConfiguration(new SetConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());
        }
    }

    public class CardConfiguration : IEntityTypeConfiguration<Card>
    {
        public void Configure(EntityTypeBuilder<Card> builder)
        {
            builder
                .ToTable("Cards");
            builder
                .Property(c => c.Name)
                .IsRequired();
            builder
                .Property(c => c.Description);
            builder
                .Property(c => c.Image);
            builder
                .Property(c => c.Power);
            builder
                .Property(c => c.Toughness);
            builder
                .Property(c => c.Types)
                .IsRequired();
            builder
                .Property(c => c.Price)
                .IsRequired();

            builder
                .HasOne(c => c.Set)
                .WithMany(s => s.Cards)
                .HasForeignKey(c => c.SetId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }

    public class CollectionConfiguration : IEntityTypeConfiguration<Collection>
    {
        public void Configure(EntityTypeBuilder<Collection> builder)
        {
            builder
                .ToTable("Collections");

            builder
                .HasKey(cl => new { cl.UserId, cl.CardId });
            builder
                .HasOne(cl => cl.Card)
                .WithMany(c => c.Collections)
                .HasForeignKey(cl => cl.CardId)
                .OnDelete(DeleteBehavior.Cascade);
            builder
                .HasOne(cl => cl.User)
                .WithMany(u => u.Collections)
                .HasForeignKey(cl => cl.UserId)
                .OnDelete(DeleteBehavior.Cascade);
            builder
                .Property(cl => cl.Count)
                .IsRequired();
        }
    }

    public class DialogueConfiguration : IEntityTypeConfiguration<Dialogue>
    {
        public void Configure(EntityTypeBuilder<Dialogue> builder)
        {
            builder.ToTable("Dialogues");

            builder
                .HasOne(d => d.User1)
                .WithMany(u => u.User1Dialogues)
                .HasForeignKey(d => d.User1Id)
                .OnDelete(DeleteBehavior.SetNull)
                .IsRequired(false);
            builder
                .HasOne(d => d.User2)
                .WithMany(u => u.User2Dialogues)
                .HasForeignKey(d => d.User2Id)
                .OnDelete(DeleteBehavior.SetNull)
                .IsRequired(false);
            builder
                .Property(d => d.CreationTime)
                .IsRequired();
        }
    }

    public class ExchangeConfiguration : IEntityTypeConfiguration<Exchange>
    {
        public void Configure(EntityTypeBuilder<Exchange> builder)
        {
            builder.ToTable("Exchanges");

            builder
                .HasOne(e => e.Dialogue)
                .WithMany(d => d.Exchanges)
                .HasForeignKey(ec => ec.DialogueId)
                .OnDelete(DeleteBehavior.Cascade);
            builder
                .HasOne(e => e.User1)
                .WithMany(u => u.User1Exchanges)
                .HasForeignKey(e => e.User1Id)
                .OnDelete(DeleteBehavior.SetNull)
                .IsRequired(false);
            builder
                .HasOne(e => e.User2)
                .WithMany(u => u.User2Exchanges)
                .HasForeignKey(e => e.User2Id)
                .OnDelete(DeleteBehavior.SetNull)
                .IsRequired(false);
            builder
                .Property(e => e.CreationTime)
                .IsRequired();
            builder
                .Property(ec => ec.Status)
                .HasConversion(
                    e => e.ToString(),
                    e => (ExchangeStatus)Enum.Parse(typeof(ExchangeStatus), e))
                .IsRequired();
        }
    }

    public class ExchangeCardConfiguration : IEntityTypeConfiguration<ExchangeCard>
    {
        public void Configure(EntityTypeBuilder<ExchangeCard> builder)
        {
            builder.ToTable("ExchangeCards");

            builder.HasKey(ec => new { ec.ExchangeId, ec.CardId, ec.UserId });

            builder
                .HasOne(ec => ec.Exchange)
                .WithMany(e => e.ExchangeCards)
                .HasForeignKey(ec => ec.ExchangeId)
                .OnDelete(DeleteBehavior.Cascade);
            builder
                .HasOne(ec => ec.Card)
                .WithMany(c => c.ExchangeCards)
                .HasForeignKey(ec => ec.CardId)
                .OnDelete(DeleteBehavior.Cascade);
            builder
                .HasOne(ec => ec.User)
                .WithMany(u => u.ExchangeCards)
                .HasForeignKey(ec => ec.UserId)
                .OnDelete(DeleteBehavior.Cascade);
            builder
                .Property(ec => ec.Count);
        }
    }

    public class MessageConfiguration : IEntityTypeConfiguration<Message>
    {
        public void Configure(EntityTypeBuilder<Message> builder)
        {
            builder.ToTable("Messages");

            builder
                .HasOne(m => m.Dialogue)
                .WithMany(d => d.Messages)
                .HasForeignKey(m => m.DialogueId)
                .OnDelete(DeleteBehavior.Cascade);
            builder
                .Property(m => m.Text)
                .IsRequired();
            builder
                .Property(m => m.Time)
                .IsRequired();
            builder
                .HasOne(m => m.User)
                .WithMany(u => u.Messages)
                .HasForeignKey(m => m.UserId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }

    public class SetConfiguration : IEntityTypeConfiguration<Set>
    {
        public void Configure(EntityTypeBuilder<Set> builder)
        {
            builder
                .ToTable("Sets");

            builder
                .Property(s => s.Name)
                .IsRequired();
            builder
                .Property(s => s.ReleaseDate)
                .IsRequired();
            builder
                .Property(s => s.Code)
                .IsRequired();
        }
    }

    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users");

            builder
                .Property(u => u.Login)
                .IsRequired();
            builder
                .HasIndex(u => u.Login)
                .IsUnique();
            builder
                .Property(u => u.Password)
                .IsRequired();

            builder
                .Property(u => u.Role)
                .HasDefaultValue(Role.User)
                .HasConversion(
                    r => r.ToString(),
                    r => (Role)Enum.Parse(typeof(Role), r))
                .IsRequired();
        }
    }
}
