﻿using CardExchangerServer.Data.Models;
using System;

namespace CardExchangerServer.Data.Resources
{
    public class ExchangeResource
    {
        public long Id { get; set; }
        public long DialogueId { get; set; }
        public long? User1Id { get; set; }
        public long? User2Id { get; set; }
        public DateTime CreationTime { get; set; }
        public ExchangeStatus Status { get; set; }
    }
}
