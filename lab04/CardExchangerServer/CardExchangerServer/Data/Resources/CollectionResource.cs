﻿namespace CardExchangerServer.Data.Resources
{
    public class CollectionResource
    {
        public long UserId { get; set; }
        public long CardId { get; set; }
        public int Count { get; set; }
    }
}
