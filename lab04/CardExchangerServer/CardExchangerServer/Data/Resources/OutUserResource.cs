﻿using CardExchangerServer.Data.Models;

namespace CardExchangerServer.Data.Resources
{
    public class OutUserResource
    {
        public long Id { get; set; }
        public string Login { get; set; }
        public Role Role { get; set; }
        public string Token { get; set; }
    }
}
