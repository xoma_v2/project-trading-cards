﻿namespace CardExchangerServer.Data.Resources
{
    public class InUserResource
    {
        public long Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
