﻿using System;

namespace CardExchangerServer.Data.Resources
{
    public class SetResource
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string Code { get; set; }
    }
}
