﻿namespace CardExchangerServer.Data.Resources
{
    public class ExchangeCardResource
    {
        public long ExchangeId { get; set; }
        public long CardId { get; set; }
        public long UserId { get; set; }
        public int Count { get; set; }
    }
}
