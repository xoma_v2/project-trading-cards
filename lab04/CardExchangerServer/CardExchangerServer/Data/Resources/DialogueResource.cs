﻿using System;

namespace CardExchangerServer.Data.Resources
{
    public class DialogueResource
    {
        public long Id { get; set; }
        public long? User1Id { get; set; }
        public long? User2Id { get; set; }
        public DateTime CreationTime { get; set; }
        public string OtherUserLogin { get; set; }
    }
}
