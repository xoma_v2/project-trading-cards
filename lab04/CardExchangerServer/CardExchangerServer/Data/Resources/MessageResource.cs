﻿using System;

namespace CardExchangerServer.Data.Resources
{
    public class MessageResource
    {
        public long Id { get; set; }
        public long DialogueId { get; set; }
        public string Text { get; set; }
        public DateTime Time { get; set; }
        public long UserId { get; set; }
    }
}
