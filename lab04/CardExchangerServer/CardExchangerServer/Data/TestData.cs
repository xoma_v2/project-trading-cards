﻿using CardExchangerServer.Data.Models;
using System.Collections.Generic;

namespace CardExchangerServer.Data
{
    public static class TestData
    {
        public static List<Dialogue> Dialogues()
        {
            return new List<Dialogue>
            {
                new Dialogue
                {

                },
                new Dialogue
                {

                },
                new Dialogue
                {

                },
                new Dialogue
                {

                },
                new Dialogue
                {

                },
            };
        }

        public static List<Message> Messages()
        {
            return new List<Message>
            {
                new Message
                {

                },
                new Message
                {

                },
                new Message
                {

                },
                new Message
                {

                },
                new Message
                {

                },
            };
        }

        public static List<Exchange> Exchanges()
        {
            return new List<Exchange>
            {
                new Exchange
                {

                },
                new Exchange
                {

                },
                new Exchange
                {

                },
                new Exchange
                {

                },
                new Exchange
                {

                },
            };
        }
    }
}
