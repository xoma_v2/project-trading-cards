﻿using System;
using System.Collections.Generic;

namespace CardExchangerServer.Data.Models
{
    public class Set
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string Code { get; set; }

        public virtual List<Card> Cards { get; set; }
    }
}
