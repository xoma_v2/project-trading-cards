﻿using System;
using System.Collections.Generic;

namespace CardExchangerServer.Data.Models
{
    public class Dialogue
    {
        public long Id { get; set; }
        public long? User1Id { get; set; }
        public long? User2Id { get; set; }
        public DateTime CreationTime { get; set; }

        public virtual User User1 { get; set; }
        public virtual User User2 { get; set; }
        public virtual List<Exchange> Exchanges { get; set; }
        public virtual List<Message> Messages { get; set; }
    }
}
