﻿using System;
using System.Collections.Generic;

namespace CardExchangerServer.Data.Models
{
    public enum ExchangeStatus
    {
        Offered,
        Accepted,
        Completed
    }

    public class Exchange
    {
        public long Id { get; set; }
        public long DialogueId { get; set; }
        public long? User1Id { get; set; }
        public long? User2Id { get; set; }
        public DateTime CreationTime { get; set; }
        public ExchangeStatus Status { get; set; }

        public virtual Dialogue Dialogue { get; set; }
        public virtual User User1 { get; set; }
        public virtual User User2 { get; set; }
        public virtual List<ExchangeCard> ExchangeCards { get; set; }
    }
}
