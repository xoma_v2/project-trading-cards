﻿namespace CardExchangerServer.Data.Models
{
    public class ExchangeCard
    {
        public long ExchangeId { get; set; }
        public long CardId { get; set; }
        public long UserId { get; set; }
        public int Count { get; set; }

        public virtual Exchange Exchange { get; set; }
        public virtual Card Card { get; set; }
        public virtual User User { get; set; }
    }
}
