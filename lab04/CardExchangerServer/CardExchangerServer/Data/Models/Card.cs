﻿using System.Collections.Generic;

namespace CardExchangerServer.Data.Models
{
    public class Card
    {
        public long Id { get; set; }
        public long SetId { get; set; }
        public string Name { get; set; }
        public string? Description { get; set; }
        public string Image { get; set; }
        public string? Mana { get; set; }
        public string? Power { get; set; }
        public string? Toughness { get; set; }
        public string[] Types { get; set; }
        public double Price { get; set; }
        public string SetCode { get; set; }

        public virtual Set Set { get; set; }
        public virtual List<ExchangeCard> ExchangeCards { get; set; }
        public virtual List<Collection> Collections { get; set; }
    }
}
