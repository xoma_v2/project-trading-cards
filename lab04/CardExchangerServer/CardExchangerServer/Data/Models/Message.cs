﻿using System;

namespace CardExchangerServer.Data.Models
{
    public class Message
    {
        public long Id { get; set; }
        public long DialogueId { get; set; }
        public string Text { get; set; }
        public DateTime Time { get; set; }
        public long UserId { get; set; }

        public virtual Dialogue Dialogue { get; set; }
        public virtual User User { get; set; }
    }
}
