﻿using System.Collections.Generic;

namespace CardExchangerServer.Data.Models
{
    public enum Role
    {
        User,
        Admin
    }

    public class User
    {
        public long Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public Role Role { get; set; }

        public virtual List<Dialogue> User1Dialogues { get; set; }
        public virtual List<Dialogue> User2Dialogues { get; set; }
        public virtual List<Exchange> User1Exchanges { get; set; }
        public virtual List<Exchange> User2Exchanges { get; set; }
        public virtual List<ExchangeCard> ExchangeCards { get; set; }
        public virtual List<Collection> Collections { get; set; }
        public virtual List<Message> Messages { get; set; }
    }
}
