﻿namespace CardExchangerServer.Data.Models
{
    public class Collection
    {
        public long UserId { get; set; }
        public long CardId { get; set; }
        public int Count { get; set; }

        public virtual User User { get; set; }
        public virtual Card Card { get; set; }
    }
}
