﻿using CardExchangerServer.Data.Models;
using CardExchangerServer.Data.Resources;
using CardExchangerServer.Interfaces.Services;
using MtgApiManager.Lib.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardExchangerServer.Data
{
    public class Generator
    {
        private readonly ICardsService _cardsService;
        private readonly ISetsService _setsService;
        private readonly IUsersService _usersService;
        private readonly IDialoguesService _dialoguesService;
        private readonly IExchangesService _exchangesService;

        public Generator(ICardsService cardsService,
                         ISetsService setsService,
                         IUsersService usersService,
                         IDialoguesService dialoguesService,
                         IExchangesService exchangesService)
        {
            _cardsService = cardsService;
            _setsService = setsService;
            _usersService = usersService;
            _dialoguesService = dialoguesService;
            _exchangesService = exchangesService;
        }

        public async Task Initialize()
        {
            if (_setsService.GetSetListAsync().Result.Object.Count() == 0)
            {
                await GenerateSets();
                await GenerateCards();
            }
            if (_usersService.GetUserListAsync(null).Result.Object.Count() == 0)
            {
                await GenerateUsers();
                await GenerateCollection();
                await GenerateDialogues();
                await GenerateMessages();
                await GenerateExchanges();
            }
        }

        public async Task GenerateCards(int countOfPages = 600) // Загрузка всех карт около 3 часов
        {
            CardService service = new CardService();
            var r = new Random();

            for (int i = 1; i <= countOfPages; i++)
            {
                var res = service
                    .Where(x => x.Page, i)
                    .Where(x => x.PageSize, 100)
                    .All();

                if (res.IsSuccess)
                {
                    foreach (var c in res.Value)
                    {
                        Uri uri = c.ImageUrl;
                        if (uri != null)
                        {
                            CardResource card = new CardResource();

                            card.Image = c.ImageUrl.ToString();
                            card.Toughness = c.Toughness;
                            card.Power = c.Power;
                            card.Description = c.Text;
                            card.Name = c.Name;
                            card.Mana = c.ManaCost;
                            card.Types = c.Types;
                            card.SetCode = c.Set;

                            var set = _setsService
                                .FindSetsAsync(c.SetName, c.Set, 0, 0)
                                .Result.Object
                                .Where(s => s.Code == c.Set)
                                .Single();

                            card.SetId = set.Id;

                            double price = 0.25;
                            switch (c.Rarity)
                            {
                                case "Uncommon":
                                    price *= r.NextDouble() * 0.5 + 1;
                                    break;
                                case "Rare":
                                    price *= r.NextDouble() * 0.6 + 1.1;
                                    break;
                                case "Mythic Rare":
                                    price *= r.NextDouble() * 0.7 + 1.2;
                                    break;
                                case "Special":
                                    price *= r.NextDouble() * 0.8 + 1.3;
                                    break;
                                case "Basic Land":
                                    price *= r.NextDouble() * 0.1 + 1;
                                    break;
                                default:
                                    price *= r.NextDouble() * 0.4 + 1;
                                    break;
                            }
                            switch (set.ReleaseDate.Year)
                            {
                                case 1993:
                                case 1994:
                                    price *= (r.NextDouble() + 1) * 3 + 1;
                                    break;
                                case 1995:
                                case 1996:
                                case 1997:
                                    price *= (r.NextDouble() + 1) * 2.5 + 1;
                                    break;
                                case 1998:
                                case 1999:
                                case 2000:
                                case 2001:
                                case 2002:
                                case 2003:
                                    price *= (r.NextDouble() + 1) * 1.5;
                                    break;
                                case 2004:
                                case 2005:
                                case 2006:
                                case 2007:
                                case 2008:
                                case 2009:
                                case 2010:
                                    price *= r.NextDouble() + 1;
                                    break;
                                default:
                                    price *= r.NextDouble() + 0.5;
                                    break;
                            }

                            card.Price = Math.Round(price, 2);

                            await _cardsService
                                .AddCardAsync(card)
                                .ConfigureAwait(true);
                        }
                    }
                }
            }
        }

        public async Task GenerateSets()
        {
            SetService servise = new SetService();
            var res = servise.All();

            if (res.IsSuccess)
            {
                foreach (var s in res.Value)
                {
                    SetResource set = new SetResource();

                    set.Name = s.Name;
                    set.ReleaseDate = DateTime.Parse(s.ReleaseDate);
                    set.Code = s.Code;
                    await _setsService
                        .AddSetAsync(set)
                        .ConfigureAwait(true);
                }
            }
        }

        public async Task GenerateUsers(int countOfUsers = 10)
        {
            var r = new Random();
            List<String> logins = new List<string>
            {
                "Nikolai",
                "Alexandr",
                "Nikita",
                "Andrey",
                "Olga",
                "Nastya",
                "Anna",
                "Sergey",
                "Victoria",
                "Vladimir"
            };

            List<String> passwords = new List<string>
            {
                "Nikolai",
                "Alexandr",
                "Nikita",
                "Andrey",
                "Olga",
                "Nastya",
                "Anna",
                "Sergey",
                "Victoria",
                "Vladimir"
            };

            for (int i = 0; i < countOfUsers; i++)
            {
                InUserResource user = new InUserResource
                {
                    Login = logins[i],
                    Password = passwords[i]
                };

                await _usersService
                    .SinginAsync(user)
                    .ConfigureAwait(true);
            }
        }

        public async Task GenerateCollection(int minCardsInCollection = 50,
                                             int maxCardsInCollection = 150,
                                             int minCopiesInCollectioint = 1,
                                             int maxCopiesInCollection = 10)
        {
            long lastCardId = _cardsService
                .GetCardListAsync()
                .Result.Object.Count();
            long lastUserId = _usersService
                .GetUserListAsync(null)
                .Result.Object.Count();
            var r = new Random();

            for (int i = 1; i <= lastUserId; i++)
            {
                if (_usersService.GetUserByIdAsync(i).Result.Object.Role == Role.User)
                {
                    for (int _ = 0; _ < r.Next(minCardsInCollection, maxCardsInCollection + 1); _++)
                    {
                        CollectionResource collection = new CollectionResource();

                        collection.UserId = i;

                        long cardId = r.Next(1, (int)lastCardId + 1);

                        var res = _cardsService
                            .GetUserCardsAsync(i)
                            .Result.Object
                            .Where(c => c.Id == cardId);

                        while (res.Count() == 1)
                        {
                            cardId = r.Next(1, (int)lastCardId + 1);

                            res = _cardsService
                                .GetUserCardsAsync(i)
                                .Result.Object
                                .Where(c => c.Id == cardId);
                        }
                        collection.CardId = cardId;
                        collection.Count = r.Next(minCopiesInCollectioint, maxCopiesInCollection + 1);

                        await _cardsService
                            .AddUserCardAsync(i, collection)
                            .ConfigureAwait(true);
                    }
                }
            }
        }

        public async Task GenerateDialogues(int minCountOfDialoguesPerUser = 0,
                                            int maxCountOfDialoguesPerUser = 3)
        {
            long lastUserId = _usersService.GetUserListAsync(null).Result.Object.Count();
            var r = new Random();

            for (int i = 1; i <= lastUserId; i++)
            {
                if (_usersService.GetUserByIdAsync(i).Result.Object.Role == Role.User)
                {
                    for (int _ = minCountOfDialoguesPerUser;
                        _ <= r.Next(minCountOfDialoguesPerUser, maxCountOfDialoguesPerUser + 1); _++)
                    {
                        long user2Id = r.Next(1, (int)lastUserId + 1);

                        var res = _dialoguesService
                            .GetUserDialoguesAsync(i)
                            .Result.Object
                            .Where(d => d.User2Id == user2Id);

                        while (res.Count() == 1)
                        {
                            user2Id = r.Next(1, (int)lastUserId + 1);

                            res = _dialoguesService
                                .GetUserDialoguesAsync(i)
                                .Result.Object
                                .Where(d => d.User2Id == user2Id);

                        }

                        DateTime start = new DateTime(2019, 1, 1);
                        int range = (DateTime.Today - start).Days;

                        DialogueResource dialogue = new DialogueResource
                        {
                            User1Id = i,
                            User2Id = user2Id,
                            CreationTime = start.AddDays(r.Next(range)).AddSeconds(r.Next(86400))
                        };

                        await _dialoguesService
                            .AddDialogueAsync(dialogue)
                            .ConfigureAwait(true);
                    }
                }
            }
        }

        public async Task GenerateExchanges(int minCountOfExchPerDialogue = 0,
                                            int maxCountOfExchPerDialogue = 4)
        {
            long lastCardId = _cardsService
                .GetCardListAsync()
                .Result.Object.Count();
            long lastUserId = _usersService
                .GetUserListAsync(null)
                .Result.Object.Count();

            var d = _dialoguesService
                .GetUserDialoguesAsync(1)
                .Result.Object.ToList();
            for (int i = 2; i <= lastUserId; i++)
            {
                d.AddRange(_dialoguesService.GetUserDialoguesAsync(i).Result.Object.ToList());
            }
            long lastDialogueId = d.Count() / 2;

            var r = new Random();

            for (int i = 1; i <= lastDialogueId; i++)
            {
                var dialogue = _dialoguesService.GetDialogueByIdAsync(i).Result.Object;

                DateTime start = dialogue.CreationTime;
                int range = (DateTime.Today - start).Days;

                ExchangeResource exchange = new ExchangeResource
                {
                    CreationTime = start.AddDays(r.Next(range)).AddSeconds(r.Next(86400)),
                    DialogueId = dialogue.Id,
                    Status = (ExchangeStatus)r.Next(Enum.GetNames(typeof(ExchangeStatus)).Length),
                    User1Id = dialogue.User1Id.Value,
                    User2Id = dialogue.User2Id.Value
                };

                await _exchangesService.AddExchangeAsync(exchange).ConfigureAwait(true);

                for (int _ = minCountOfExchPerDialogue; _ <= maxCountOfExchPerDialogue; _++)
                {
                    long userId = (r.Next(0, 2) == 0) ? exchange.User1Id.Value : exchange.User2Id.Value;
                    long cardId = r.Next(1, (int)lastCardId + 1);

                    var t = _exchangesService
                        .GetExchangeCardsAsync(exchange.Id)
                        .Result.Object
                        .ToList()
                        .Where(ec => ec.CardId == cardId && ec.UserId == userId);

                    while (t.Count() == 1)
                    {
                        userId = (r.Next(0, 2) == 0) ? exchange.User1Id.Value : exchange.User2Id.Value;
                        cardId = r.Next(1, (int)lastCardId + 1);
                        t = _exchangesService
                            .GetExchangeCardsAsync(exchange.Id)
                            .Result.Object
                            .ToList()
                            .Where(ec => ec.CardId == cardId && ec.UserId == userId);
                    }

                    ExchangeCardResource exchangeCard = new ExchangeCardResource
                    {
                        ExchangeId = exchange.Id,
                        CardId = cardId,
                        Count = r.Next(1, 6),
                        UserId = userId
                    };

                    await _exchangesService
                        .AddExchangeCardAsync(exchange.Id, exchangeCard)
                        .ConfigureAwait(true);
                }
            }
        }

        public async Task GenerateMessages(int minCountOfMesPerDialogue = 10,
                                           int maxCountOfMesPerDialogue = 30)
        {
            long lastUserId = _usersService
                .GetUserListAsync(null)
                .Result.Object.Count();
            var d = _dialoguesService
                .GetUserDialoguesAsync(1)
                .Result.Object
                .ToList();
            for (int i = 2; i <= lastUserId; i++)
            {
                d.AddRange(_dialoguesService.GetUserDialoguesAsync(i).Result.Object.ToList());
            }

            long lastDialogueId = d.Count() / 2;
            var r = new Random();

            for (int i = 1; i <= lastDialogueId; i++)
            {
                var dialogue = _dialoguesService.GetDialogueByIdAsync(i).Result.Object;

                for (int _ = minCountOfMesPerDialogue;
                    _ <= r.Next(minCountOfMesPerDialogue, maxCountOfMesPerDialogue + 1); _++)
                {
                    DateTime start = dialogue.CreationTime;
                    int range = (DateTime.Today - start).Days;

                    MessageResource message = new MessageResource
                    {
                        Text = string.Concat(Enumerable.Repeat("Some text ", r.Next(1, 11))),
                        Time = start.AddDays(r.Next(range)).AddSeconds(r.Next(86400)),
                        UserId = (r.Next(0, 2) == 0) ? dialogue.User1Id.Value : dialogue.User2Id.Value
                    };
                    await _dialoguesService
                        .AddMessageAsync(dialogue.Id, message)
                        .ConfigureAwait(true);
                }
            }
        }
    }
}
