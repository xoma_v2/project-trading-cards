﻿using AutoMapper;
using CardExchangerServer.Data.Models;
using CardExchangerServer.Data.Resources;

namespace CardExchangerServer.Data
{
    public class ModelToResourceProfile : Profile
    {
        public ModelToResourceProfile()
        {
            CreateMap<Card, CardResource>();
            CreateMap<Dialogue, DialogueResource>();
            CreateMap<Message, MessageResource>();
            CreateMap<Set, SetResource>();
            CreateMap<Exchange, ExchangeResource>();
            CreateMap<ExchangeCard, ExchangeCardResource>();
            CreateMap<Collection, CollectionResource>();

            CreateMap<CardResource, Card>();
            CreateMap<DialogueResource, Dialogue>();
            CreateMap<MessageResource, Message>();
            CreateMap<SetResource, Set>();
            CreateMap<ExchangeResource, Exchange>();
            CreateMap<ExchangeCardResource, ExchangeCard>();
            CreateMap<CollectionResource, Collection>();

            CreateMap<User, OutUserResource>();
            CreateMap<InUserResource, User>();
        }
    }
}