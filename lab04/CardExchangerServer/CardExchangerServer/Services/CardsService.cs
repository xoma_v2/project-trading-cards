﻿using AutoMapper;
using CardExchangerServer.Communication;
using CardExchangerServer.Data.Models;
using CardExchangerServer.Data.Resources;
using CardExchangerServer.Interfaces;
using CardExchangerServer.Interfaces.Repositories;
using CardExchangerServer.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardExchangerServer.Services
{
    public class CardsService : ICardsService
    {
        private readonly ICardsRepository _cardsRepository;
        private readonly IUsersRepository _usersRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CardsService(ICardsRepository cardsRepository,
                            IUsersRepository usersRepository,
                            IUnitOfWork unitOfWork,
                            IMapper mapper)
        {
            _cardsRepository = cardsRepository;
            _usersRepository = usersRepository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<ObjectResponse<IEnumerable<CardResource>>> GetCardListAsync()
        {
            try
            {
                var cards = await _cardsRepository.CardListAsync().ConfigureAwait(true);

                var result = _mapper.Map<IEnumerable<Card>, IEnumerable<CardResource>>(cards);

                return new ObjectResponse<IEnumerable<CardResource>>(result);
            }
            catch (Exception ex)
            {
                return new ObjectResponse<IEnumerable<CardResource>>(
                    $"Error occured while getting Card list: {ex.Message}", 500);
            }
        }

        public async Task<ObjectResponse<IEnumerable<CardResource>>> GetSetCardListAsync(long id)
        {
            try
            {
                var cards = await _cardsRepository.SetCardListAsync(id).ConfigureAwait(true);

                var result = _mapper.Map<IEnumerable<Card>, IEnumerable<CardResource>>(cards);

                return new ObjectResponse<IEnumerable<CardResource>>(result);
            }
            catch (Exception ex)
            {
                return new ObjectResponse<IEnumerable<CardResource>>(
                    $"Error occured while getting Set: {ex.Message}", 500);
            }
        }

        public async Task<ObjectResponse<IEnumerable<CardResource>>> FindCardsAsync(string? cardText,
                                                           string? setCode,
                                                           string? cardType,
                                                           int offset = 0,
                                                           int limit = 0,
                                                           bool onlyCount = false)
        {
            if (offset < 0 || limit < 0)
            {
                return new ObjectResponse<IEnumerable<CardResource>>($"Wrong query parameters", 400);
            }

            try
            {
                IEnumerable<Card> cards = null;

                if (!string.IsNullOrWhiteSpace(cardText) &&
                    !string.IsNullOrWhiteSpace(setCode) &&
                    !string.IsNullOrWhiteSpace(cardType))
                {
                    var cards1 = await _cardsRepository.FindByTextAsync(cardText).ConfigureAwait(true);
                    var cards2 = await _cardsRepository.FindBySetCodeAsync(setCode).ConfigureAwait(true);
                    var cards3 = await _cardsRepository.FindByCardTypeAsync(cardType).ConfigureAwait(true);
                    cards = cards1.Intersect(cards2).Intersect(cards3);
                }
                else if (!string.IsNullOrWhiteSpace(cardText) && !string.IsNullOrWhiteSpace(setCode))
                {
                    var cards1 = await _cardsRepository.FindByTextAsync(cardText).ConfigureAwait(true);
                    var cards2 = await _cardsRepository.FindBySetCodeAsync(setCode).ConfigureAwait(true);
                    cards = cards1.Intersect(cards2);
                }
                else if (!string.IsNullOrWhiteSpace(setCode) && !string.IsNullOrWhiteSpace(cardType))
                {
                    var cards1 = await _cardsRepository.FindBySetCodeAsync(setCode).ConfigureAwait(true);
                    var cards2 = await _cardsRepository.FindByCardTypeAsync(cardType).ConfigureAwait(true);
                    cards = cards1.Intersect(cards2);
                }
                else if (!string.IsNullOrWhiteSpace(cardText) && !string.IsNullOrWhiteSpace(cardType))
                {
                    var cards1 = await _cardsRepository.FindByTextAsync(cardText).ConfigureAwait(true);
                    var cards2 = await _cardsRepository.FindByCardTypeAsync(cardType).ConfigureAwait(true);
                    cards = cards1.Intersect(cards2);
                }
                else if (!string.IsNullOrWhiteSpace(cardText))
                {
                    cards = await _cardsRepository.FindByTextAsync(cardText).ConfigureAwait(true);
                }
                else if (!string.IsNullOrWhiteSpace(setCode))
                {
                    cards = await _cardsRepository.FindBySetCodeAsync(setCode).ConfigureAwait(true);
                }
                else if (!string.IsNullOrWhiteSpace(cardType))
                {
                    cards = await _cardsRepository.FindByCardTypeAsync(cardType).ConfigureAwait(true);
                }
                else
                {
                    cards = await _cardsRepository.CardListAsync().ConfigureAwait(true);
                }

                cards = (limit != 0) ? cards.Skip(offset).Take(limit) : cards.Skip(offset);

                if (onlyCount)
                {
                    var response = new ObjectResponse<IEnumerable<CardResource>>(null);
                    response.Count = cards.Count();

                    return response;
                }

                var result = _mapper.Map<IEnumerable<Card>, IEnumerable<CardResource>>(cards);

                return new ObjectResponse<IEnumerable<CardResource>>(result);
            }
            catch (Exception ex)
            {
                return new ObjectResponse<IEnumerable<CardResource>>(
                    $"Error occured while searching cards: {ex.Message}", 500);
            }
        }

        public async Task<ObjectResponse<CardResource>> GetCardByIdAsync(long id)
        {
            try
            {
                var card = await _cardsRepository.GetByIdAsync(id).ConfigureAwait(true);

                if (card == null)
                {
                    return new ObjectResponse<CardResource>($"Card with specified id not found", 404);
                }

                var result = _mapper.Map<Card, CardResource>(card);

                return new ObjectResponse<CardResource>(result);
            }
            catch (Exception ex)
            {
                return new ObjectResponse<CardResource>($"Error occured while getting Card: {ex.Message}", 500);
            }
        }

        public async Task<ObjectResponse<CardResource>> AddCardAsync(CardResource resource)
        {
            try
            {
                var card = _mapper.Map<CardResource, Card>(resource);

                ValidateCard(card);

                await _cardsRepository.AddAsync(card).ConfigureAwait(true);
                await _unitOfWork.CompleteAsync().ConfigureAwait(true);

                var result = _mapper.Map<Card, CardResource>(card);

                return new ObjectResponse<CardResource>(result);

            }
            catch (Exception ex) when (ex is ArgumentException || ex is ArgumentNullException)
            {
                return new ObjectResponse<CardResource>($"Error occured while adding Card: {ex.Message}", 400);
            }
            catch (Exception ex)
            {
                return new ObjectResponse<CardResource>($"Error occured while adding Card: {ex.Message}", 500);
            }
        }

        public async Task<ObjectResponse<CardResource>> UpdateCardAsync(CardResource resource)
        {
            try
            {
                var card = _mapper.Map<CardResource, Card>(resource);

                ValidateCard(card);

                var temp = await _cardsRepository.GetByIdAsync(card.Id).ConfigureAwait(true);

                if (temp == null)
                {
                    return new ObjectResponse<CardResource>($"Set with specified id not found", 404);
                }

                _cardsRepository.Update(card);
                await _unitOfWork.CompleteAsync().ConfigureAwait(true);

                var result = _mapper.Map<Card, CardResource>(card);

                return new ObjectResponse<CardResource>(result);
            }
            catch (Exception ex) when (ex is ArgumentException || ex is ArgumentNullException)
            {
                return new ObjectResponse<CardResource>($"Error occured while updating Card: {ex.Message}", 400);
            }
            catch (Exception ex)
            {
                return new ObjectResponse<CardResource>($"Error occured while updating Card: {ex.Message}", 500);
            }
        }

        public async Task<EmptyResponse> DeleteCardByIdAsync(long id)
        {
            try
            {
                Card card;

                card = await _cardsRepository.GetByIdAsync(id).ConfigureAwait(true);

                if (card == null)
                {
                    return new EmptyResponse($"Card with specified id not found", 404);
                }

                _cardsRepository.Remove(card);
                await _unitOfWork.CompleteAsync().ConfigureAwait(true);

                return new EmptyResponse();
            }
            catch (Exception ex)
            {
                return new EmptyResponse($"Error occured while deleting Card: {ex.Message}", 500);
            }
        }

        public async Task<ObjectResponse<IEnumerable<CardResource>>> GetUserCardsAsync(long id, int offset = 0, int limit = 0)
        {
            if (offset < 0 || limit < 0)
            {
                return new ObjectResponse<IEnumerable<CardResource>>("Wrong query parameters", 400);
            }

            try
            {
                var cards = await _cardsRepository.UserCardListAsync(id).ConfigureAwait(true);

                var result = _mapper.Map<IEnumerable<Card>, IEnumerable<CardResource>>(
                    (limit != 0) ?
                    cards.Skip(offset).Take(limit) :
                    cards.Skip(offset));

                foreach (var card in result)
                {
                    var collection = await _cardsRepository
                        .GetCollectionByIdAsync(id, card.Id).ConfigureAwait(true);
                    card.Count = collection.Count;
                }

                return new ObjectResponse<IEnumerable<CardResource>>(result);
            }
            catch (Exception ex)
            {
                return new ObjectResponse<IEnumerable<CardResource>>(
                    $"Error occured while getting user cards: {ex.Message}", 500);
            }
        }

        public async Task<ObjectResponse<CollectionResource>>
            AddUserCardAsync(long userId, CollectionResource resource)
        {
            try
            {
                var collection = _mapper.Map<CollectionResource, Collection>(resource);
                ValidateCollection(collection);

                var user = await _usersRepository.GetByIdAsync(userId).ConfigureAwait(true);
                if (user == null)
                {
                    return new ObjectResponse<CollectionResource>($"User with specified id not found", 404);
                }

                var card = await _cardsRepository.GetByIdAsync(collection.CardId).ConfigureAwait(true);
                if (user == null)
                {
                    return new ObjectResponse<CollectionResource>($"Card with specified id not found", 404);
                }

                var col = await _cardsRepository
                    .GetCollectionByIdAsync(collection.UserId, collection.CardId).ConfigureAwait(true);
                if (col == null)
                {
                    await _cardsRepository.AddCardToCollectionAsync(collection);
                    col = collection;
                }
                else
                {
                    col.Count += collection.Count;
                    _cardsRepository.UpdateCollection(col);
                }

                await _unitOfWork.CompleteAsync().ConfigureAwait(true);

                var result = _mapper.Map<Collection, CollectionResource>(col);

                return new ObjectResponse<CollectionResource>(result);
            }
            catch (Exception ex) when (ex is ArgumentNullException || ex is ArgumentException)
            {
                return new ObjectResponse<CollectionResource>(
                    $"Error occcured while adding card to user collection: {ex.Message}", 400);
            }
            catch (Exception ex)
            {
                return new ObjectResponse<CollectionResource>(
                    $"Error occured while adding card to user collection: {ex.Message}", 500);
            }
        }

        public async Task<EmptyResponse> DeleteUserCardAsync(long userId, CollectionResource resource)
        {
            try
            {
                var collection = _mapper.Map<CollectionResource, Collection>(resource);
                ValidateCollection(collection);

                var user = await _usersRepository.GetByIdAsync(userId).ConfigureAwait(true);
                if (user == null)
                {
                    return new EmptyResponse($"User with specified id not found", 404);
                }

                var col = await _cardsRepository
                    .GetCollectionByIdAsync(collection.UserId, collection.CardId).ConfigureAwait(true);

                if (col == null)
                {
                    return new EmptyResponse("Card with specified id not found in user collection", 404);
                }

                if (collection.Count < col.Count)
                {
                    col.Count -= collection.Count;
                    _cardsRepository.UpdateCollection(col);
                }
                else if (collection.Count == col.Count)
                {
                    _cardsRepository.RemoveCardFromCollection(col);
                }
                else
                {
                    return new EmptyResponse($"To much cards to delete.", 400);
                }

                await _unitOfWork.CompleteAsync().ConfigureAwait(true);

                return new EmptyResponse();
            }
            catch (Exception ex) when (ex is ArgumentNullException || ex is ArgumentException)
            {
                return new EmptyResponse($"Error occcured while deleting card from user collection: {ex.Message}", 400);
            }
            catch (Exception ex)
            {
                return new EmptyResponse($"Error occured while deleting card from user collection: {ex.Message}", 500);
            }
        }

        private void ValidateCollection(Collection collection)
        {
            if (collection.Count < 1)
            {
                throw new ArgumentException("Number of cards can't be less than 1.");
            }
        }


        private void ValidateCard(Card card)
        {
            if (card == null)
            {
                throw new ArgumentNullException(nameof(card));
            }

            if (string.IsNullOrWhiteSpace(card.Name))
            {
                throw new ArgumentException("Card name can't be empty.");
            }

            if (string.IsNullOrWhiteSpace(card.Description))
            {
                throw new ArgumentException("Card description can't be empty.");
            }

            if (string.IsNullOrWhiteSpace(card.Mana))
            {
                throw new ArgumentException("Card mana field can't be empty.");
            }

            if (card.Price < 0)
            {
                throw new ArgumentException("Card price can't be negative.");
            }

            if (card.Image == null)
            {
                throw new ArgumentNullException(nameof(card.Image));
            }
        }
    }
}
