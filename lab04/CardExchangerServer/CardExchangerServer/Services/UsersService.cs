﻿using AutoMapper;
using CardExchangerServer.Communication;
using CardExchangerServer.Controllers;
using CardExchangerServer.Data.Models;
using CardExchangerServer.Data.Resources;
using CardExchangerServer.Interfaces;
using CardExchangerServer.Interfaces.Repositories;
using CardExchangerServer.Interfaces.Services;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CardExchangerServer.Services
{
    public class UsersService : IUsersService
    {
        private readonly IUsersRepository _usersRepository;
        private readonly ICardsRepository _cardsRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UsersService(IUsersRepository usersRepository,
                            ICardsRepository cardsRepository,
                            IUnitOfWork unitOfWork,
                            IMapper mapper)
        {
            _usersRepository = usersRepository;
            _cardsRepository = cardsRepository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<ObjectResponse<OutUserResource>> LoginAsync(InUserResource resource)
        {
            try
            {
                var user = _mapper.Map<InUserResource, User>(resource);
                ValidateUser(user);

                var usr = await _usersRepository.GetByLoginAsync(user.Login).ConfigureAwait(true);
                if (usr == null)
                {
                    return new ObjectResponse<OutUserResource>(
                        $"User with specified credentials not found", 400);
                }

                var password = GetHashString(user.Password);
                if (password != usr.Password)
                {
                    return new ObjectResponse<OutUserResource>(
                    $"User with specified credentials not found", 400);
                }

                string token = GetToken(usr);
                var result = _mapper.Map<User, OutUserResource>(usr);
                result.Token = token;

                return new ObjectResponse<OutUserResource>(result);
            }
            catch (Exception ex) when (ex is ArgumentException || ex is ArgumentNullException)
            {
                return new ObjectResponse<OutUserResource>(
                    $"Error occured while logging in: {ex.Message}", 400);
            }
            catch (Exception ex)
            {
                return new ObjectResponse<OutUserResource>(
                    $"Error occured while logging in: {ex.Message}", 500);
            }
        }

        public async Task<ObjectResponse<OutUserResource>> SinginAsync(InUserResource resource)
        {
            try
            {
                var user = _mapper.Map<InUserResource, User>(resource);
                ValidateUser(user);

                var usr = await _usersRepository.GetByLoginAsync(user.Login).ConfigureAwait(true);
                if (usr != null) 
                {
                    return new ObjectResponse<OutUserResource>(
                        $"User with specified login already exists", 400);
                }

                user.Password = GetHashString(user.Password);

                await _usersRepository.AddAsync(user).ConfigureAwait(true);
                await _unitOfWork.CompleteAsync().ConfigureAwait(true);

                var result = _mapper.Map<User, OutUserResource>(user);

                return new ObjectResponse<OutUserResource>(result);
            }
            catch (Exception ex) when (ex is ArgumentException || ex is ArgumentNullException)
            {
                return new ObjectResponse<OutUserResource>(
                    $"Error occured while singing in: {ex.Message}", 400);
            }
            catch (Exception ex)
            {
                return new ObjectResponse<OutUserResource>(
                    $"Error occured while singing in: {ex.Message}", 500);
            }
        }

        public async Task<ObjectResponse<IEnumerable<OutUserResource>>> GetUserListAsync(string? login)
        {
            try
            {
                IEnumerable<User> users = null;

                if (!string.IsNullOrWhiteSpace(login))
                {
                    users = await _usersRepository.FindByLoginAsync(login).ConfigureAwait(true);
                }
                else
                {
                    users = await _usersRepository.ListAsync().ConfigureAwait(true);
                }

                var result = _mapper.Map<IEnumerable<User>, IEnumerable<OutUserResource>>(users);

                return new ObjectResponse<IEnumerable<OutUserResource>>(result);
            }
            catch (Exception ex)
            {
                return new ObjectResponse<IEnumerable<OutUserResource>>(
                    $"Error occured while getting list of users: {ex.Message}", 500);
            }
        }

        public async Task<ObjectResponse<OutUserResource>> GetUserByIdAsync(long id)
        {
            try
            {
                var user = await _usersRepository.GetByIdAsync(id).ConfigureAwait(true);
                if (user == null)
                {
                    return new ObjectResponse<OutUserResource>(
                        $"User with specified id not found", 404);
                }

                var result = _mapper.Map<User, OutUserResource>(user);

                return new ObjectResponse<OutUserResource>(result);
            }
            catch (Exception ex)
            {
                return new ObjectResponse<OutUserResource>(
                    $"Error occured while getting user by id: {ex.Message}", 500);
            }
        }

        public async Task<EmptyResponse> DeleteUserByIdAsync(long id)
        {
            try
            {
                var user = await _usersRepository.GetByIdAsync(id).ConfigureAwait(true);
                if (user == null)
                {
                    return new EmptyResponse($"User with specified id not found", 404);
                }

                _usersRepository.Remove(user);
                await _unitOfWork.CompleteAsync().ConfigureAwait(true);

                return new EmptyResponse();
            }
            catch (Exception ex)
            {
                return new EmptyResponse($"Error occured while deleting user: {ex.Message}", 500);
            }
        }

        public async Task<ObjectResponse<OutUserResource>> UpdateUserAsync(InUserResource resource)
        {
            try
            {
                var user = _mapper.Map<InUserResource, User>(resource);
                ValidateUser(user);

                var temp = await _usersRepository.GetByIdAsync(user.Id).ConfigureAwait(true);
                if (temp == null)
                {
                    return new ObjectResponse<OutUserResource>(
                        $"User with specified id not found", 404);
                }

                _usersRepository.Update(user);
                await _unitOfWork.CompleteAsync().ConfigureAwait(true);

                var result = _mapper.Map<User, OutUserResource>(user);

                return new ObjectResponse<OutUserResource>(result);
            }
            catch (Exception ex) when (ex is ArgumentException || ex is ArgumentNullException)
            {
                return new ObjectResponse<OutUserResource>(
                    $"Error occured while updating User: {ex.Message}", 400);
            }
            catch (Exception ex)
            {
                return new ObjectResponse<OutUserResource>(
                    $"Error occured while updating User: {ex.Message}", 500);
            }
        }

        private void ValidateUser(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            if (string.IsNullOrWhiteSpace(user.Login))
            {
                throw new ArgumentException("Login can't be empty.");
            }

            if (string.IsNullOrWhiteSpace(user.Password))
            {
                throw new ArgumentException("Password can't be empty.");
            }
        }

        private string GetToken(User user)
        {
            var identity = GetIdentity(user);

            if (identity == null)
            {
                throw new ArgumentNullException(nameof(identity));
            }

            var now = DateTime.UtcNow;

            var jwt = new JwtSecurityToken(
                    issuer: AuthOptions.ISSUER,
                    audience: AuthOptions.AUDIENCE,
                    notBefore: now,
                    claims: identity.Claims,
                    expires: now.Add(TimeSpan.FromDays(AuthOptions.JWT_TOKEN_LIFETIME)),
                    signingCredentials: new SigningCredentials(
                        AuthOptions.GetSymmetricSecurityKey(),
                        SecurityAlgorithms.HmacSha256));
            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }

        private ClaimsIdentity GetIdentity(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Login),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role.ToString()),
            };

            ClaimsIdentity claimsIdentity =
                new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                    ClaimsIdentity.DefaultRoleClaimType);

            return claimsIdentity;
        }

        public static string GetHashString(string password)
        {
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: Convert.FromBase64String("NOac9EX1eZLoiD+Te5cKSg=="),
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));

            return hashed;
        }

    }
}
