﻿using AutoMapper;
using CardExchangerServer.Communication;
using CardExchangerServer.Data.Models;
using CardExchangerServer.Data.Resources;
using CardExchangerServer.Interfaces;
using CardExchangerServer.Interfaces.Repositories;
using CardExchangerServer.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardExchangerServer.Services
{
    public class DialoguesService : IDialoguesService
    {
        private readonly IDialoguesRepository _dialoguesRepository;
        private readonly IUsersRepository _usersRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public DialoguesService(IDialoguesRepository dialoguesRepository,
                                IUsersRepository usersRepository,
                                IUnitOfWork unitOfWork,
                                IMapper mapper)
        {
            _dialoguesRepository = dialoguesRepository;
            _usersRepository = usersRepository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<ObjectResponse<IEnumerable<DialogueResource>>> GetUserDialoguesAsync(long userId)
        {
            try
            {
                var dialogues = await _dialoguesRepository
                    .GetUserDialoguesAsync(userId).ConfigureAwait(true);

                var result = _mapper
                    .Map<IEnumerable<Dialogue>, IEnumerable<DialogueResource>>(dialogues);

                foreach (var dialogue in result)
                {
                    if (dialogue.User1Id.HasValue && dialogue.User2Id.HasValue)
                    {
                        var user = await _usersRepository
                        .GetByIdAsync(
                            (dialogue.User1Id.Value == userId) ?
                            dialogue.User2Id.Value :
                            dialogue.User1Id.Value).
                        ConfigureAwait(true);
                        dialogue.OtherUserLogin = user.Login;
                    }
                    else
                    {
                        dialogue.OtherUserLogin = "deleted";
                    }
                }

                return new ObjectResponse<IEnumerable<DialogueResource>>(result);
            }
            catch (Exception e)
            {
                return new ObjectResponse<IEnumerable<DialogueResource>>($"{e.Message}", 500);
            }
        }

        public async Task<ObjectResponse<DialogueResource>> GetDialogueByIdAsync(long id)
        {
            try
            {
                var dialogue = await _dialoguesRepository
                    .GetDialogueByIdAsync(id).ConfigureAwait(true);
                if (dialogue == null)
                {
                    return new ObjectResponse<DialogueResource>(
                        "Dialogue with specified id not found", 404);
                }

                var result = _mapper.Map<Dialogue, DialogueResource>(dialogue);

                return new ObjectResponse<DialogueResource>(result);
            }
            catch (Exception e)
            {
                return new ObjectResponse<DialogueResource>($"{e.Message}", 500);
            }
        }

        public async Task<ObjectResponse<DialogueResource>> AddDialogueAsync(DialogueResource resource)
        {
            try
            {
                var dialogue = _mapper.Map<DialogueResource, Dialogue>(resource);
                ValidateDialogue(dialogue);

                var d = await _dialoguesRepository
                    .GetDialogueByUsersIdAsync(dialogue.User1Id.Value, dialogue.User2Id.Value)
                    .ConfigureAwait(true);
                if (d != null)
                {
                    return new ObjectResponse<DialogueResource>(
                        _mapper.Map<Dialogue, DialogueResource>(d));
                }

                var user = await _usersRepository
                    .GetByIdAsync(dialogue.User1Id.Value).ConfigureAwait(true);
                if (user == null)
                {
                    return new ObjectResponse<DialogueResource>(
                        "First user not found", 404);
                }
                user = await _usersRepository
                    .GetByIdAsync(dialogue.User2Id.Value).ConfigureAwait(true);
                if (user == null)
                {
                    return new ObjectResponse<DialogueResource>(
                        "Second user not found", 404);
                }

                dialogue.CreationTime = DateTime.Now;

                await _dialoguesRepository.AddDialogueAsync(dialogue);
                await _unitOfWork.CompleteAsync().ConfigureAwait(true);

                var result = _mapper.Map<Dialogue, DialogueResource>(dialogue);

                return new ObjectResponse<DialogueResource>(result);
            }
            catch (Exception e) when (e is ArgumentNullException || e is ArgumentException)
            {
                return new ObjectResponse<DialogueResource>($"{e.Message}", 400);
            }
            catch (Exception e)
            {
                return new ObjectResponse<DialogueResource>($"{e.Message}", 500);
            }
        }

        public async Task<EmptyResponse> DeleteDialogueAsync(long id)
        {
            try
            {
                var dialogue = await _dialoguesRepository
                    .GetDialogueByIdAsync(id).ConfigureAwait(true);
                if (dialogue == null)
                {
                    return new EmptyResponse(
                        "Dialogue with specified id not found", 404);
                }

                _dialoguesRepository.RemoveDialogue(dialogue);
                await _unitOfWork.CompleteAsync().ConfigureAwait(true);

                return new EmptyResponse();
            }
            catch (Exception e)
            {
                return new EmptyResponse($"{e.Message}", 500);
            }
        }

        public async Task<ObjectResponse<IEnumerable<MessageResource>>> GetDialogueMessagesAsync(long dialogueId)
        {
            try
            {
                var messages = await _dialoguesRepository
                    .GetDialogueMessagesAsync(dialogueId).ConfigureAwait(true);

                var result = _mapper
                    .Map<IEnumerable<Message>, IEnumerable<MessageResource>>(messages);

                return new ObjectResponse<IEnumerable<MessageResource>>(result);
            }
            catch (Exception e)
            {
                return new ObjectResponse<IEnumerable<MessageResource>>($"{e.Message}", 500);
            }
        }

        public async Task<ObjectResponse<MessageResource>> GetLastDialogueMessageAsync(long dialogueId)
        {
            try
            {
                var messages = await _dialoguesRepository
                    .GetDialogueMessagesAsync(dialogueId).ConfigureAwait(true);
                if (messages.Count() == 0)
                {
                    return new ObjectResponse<MessageResource>(null);
                }

                var message = messages.Last();

                foreach (var msg in messages)
                {
                    if (msg.Time > message.Time)
                    {
                        message = msg;
                    }
                }

                var result = _mapper.Map<Message, MessageResource>(message);

                return new ObjectResponse<MessageResource>(result);
            }
            catch (Exception e)
            {
                return new ObjectResponse<MessageResource>($"{e.Message}", 500);
            }
        }

        public async Task<ObjectResponse<MessageResource>> AddMessageAsync(long dialogueId, MessageResource resource)
        {
            try
            {
                var dialogue = await _dialoguesRepository
                    .GetDialogueByIdAsync(dialogueId).ConfigureAwait(true);
                if (dialogue == null)
                {
                    return new ObjectResponse<MessageResource>(
                        "Dialogue with specified id not found", 400);
                }
                else if (!dialogue.User1Id.HasValue || !dialogue.User2Id.HasValue)
                {
                    return new ObjectResponse<MessageResource>(
                        "One of participants was deleted", 400);
                }

                var message = _mapper.Map<MessageResource, Message>(resource);
                ValidateMessage(message);

                var user = await _usersRepository
                    .GetByIdAsync(message.UserId).ConfigureAwait(true);
                if (user == null)
                {
                    return new ObjectResponse<MessageResource>(
                        "User with specified id not found", 400);
                }
                else if (user.Id != dialogue.User1Id.Value && user.Id != dialogue.User2Id.Value)
                {
                    return new ObjectResponse<MessageResource>(
                        "User with specified id does not take part in current dialogue", 400);
                }

                message.DialogueId = dialogueId;
                message.Time = DateTime.Now;

                await _dialoguesRepository.AddMessageAsync(message).ConfigureAwait(true);
                await _unitOfWork.CompleteAsync().ConfigureAwait(true);

                var result = _mapper.Map<Message, MessageResource>(message);

                return new ObjectResponse<MessageResource>(result);
            }
            catch (Exception e) when (e is ArgumentNullException || e is ArgumentException)
            {
                return new ObjectResponse<MessageResource>($"{e.Message}", 400);
            }
            catch (Exception e)
            {
                return new ObjectResponse<MessageResource>($"{e.Message}", 500);
            }
        }

        public async Task<ObjectResponse<MessageResource>> UpdateMessageAsync(long dialogueId, MessageResource resource)
        {
            try
            {
                var dialogue = await _dialoguesRepository
                    .GetDialogueByIdAsync(dialogueId).ConfigureAwait(true);
                if (dialogue == null)
                {
                    return new ObjectResponse<MessageResource>(
                        "Dialogue with specified id not found", 400);
                }

                var message = _mapper.Map<MessageResource, Message>(resource);
                ValidateMessage(message);

                var msg = await _dialoguesRepository
                    .GetMessageByIdAsync(message.Id).ConfigureAwait(true);
                if (msg == null)
                {
                    return new ObjectResponse<MessageResource>(
                        "Message with specified id not found", 400);
                }

                msg.Text = message.Text;
                msg.Time = DateTime.Now;

                _dialoguesRepository.UpdateMessage(msg);
                await _unitOfWork.CompleteAsync().ConfigureAwait(true);

                var result = _mapper.Map<Message, MessageResource>(msg);

                return new ObjectResponse<MessageResource>(result);
            }
            catch (Exception e) when (e is ArgumentNullException || e is ArgumentException)
            {
                return new ObjectResponse<MessageResource>($"{e.Message}", 400);
            }
            catch (Exception e)
            {
                return new ObjectResponse<MessageResource>($"{e.Message}", 500);
            }
        }

        public async Task<EmptyResponse> DeleteMessageAsync(long dialogueId, long messageId)
        {
            try
            {
                var dialogue = await _dialoguesRepository
                    .GetDialogueByIdAsync(dialogueId).ConfigureAwait(true);
                if (dialogue == null)
                {
                    return new EmptyResponse("Dialogue with specified id not found", 400);
                }

                var message = await _dialoguesRepository
                    .GetMessageByIdAsync(messageId).ConfigureAwait(true);
                if (message == null)
                {
                    return new EmptyResponse("Message with specified id not found", 400);
                }

                if (message.DialogueId != dialogue.Id)
                {
                    return new EmptyResponse(
                        "Message with specified id not belongs to current dialogue", 400);
                }

                _dialoguesRepository.RemoveMessage(message);
                await _unitOfWork.CompleteAsync().ConfigureAwait(true);

                return new EmptyResponse();
            }
            catch (Exception e)
            {
                return new EmptyResponse($"{e.Message}", 500);
            }
        }

        private void ValidateDialogue(Dialogue dialogue)
        {
            if (dialogue == null)
            {
                throw new ArgumentNullException(nameof(dialogue));
            }

            if (!dialogue.User1Id.HasValue)
            {
                throw new ArgumentNullException(nameof(dialogue.User1Id));
            }

            if (!dialogue.User2Id.HasValue)
            {
                throw new ArgumentNullException(nameof(dialogue.User2Id));
            }
        }

        private void ValidateMessage(Message message)
        {
            if (message == null)
            {
                throw new ArgumentNullException(nameof(message));
            }

            if (string.IsNullOrWhiteSpace(message.Text))
            {
                throw new ArgumentException("Message content can't be empty of white space.");
            }
        }
    }
}
