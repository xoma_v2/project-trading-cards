﻿using AutoMapper;
using CardExchangerServer.Communication;
using CardExchangerServer.Data.Models;
using CardExchangerServer.Data.Resources;
using CardExchangerServer.Interfaces;
using CardExchangerServer.Interfaces.Repositories;
using CardExchangerServer.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardExchangerServer.Services
{
    public class SetsService : ISetsService
    {
        private readonly ISetsRepository _setsRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public SetsService(ISetsRepository setsRepository, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _setsRepository = setsRepository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<ObjectResponse<IEnumerable<SetResource>>> GetSetListAsync()
        {
            try
            {
                var sets = await _setsRepository.ListAsync().ConfigureAwait(true);

                var result = _mapper.Map<IEnumerable<Set>, IEnumerable<SetResource>>(sets);

                return new ObjectResponse<IEnumerable<SetResource>>(result);
            }
            catch (Exception ex)
            {
                return new ObjectResponse<IEnumerable<SetResource>>(
                    $"Error occured while getting Set list: {ex.Message}", 500);
            }
        }

        public async Task<ObjectResponse<IEnumerable<SetResource>>> FindSetsAsync(string? setName,
                                                                                  string? setCode,
                                                                                  int offset = 0,
                                                                                  int limit = 0,
                                                                                  bool onlyCount = false)
        {
            if (offset < 0 || limit < 0)
            {
                return new ObjectResponse<IEnumerable<SetResource>>("Wrong query parameters", 400);
            }

            try
            {
                IEnumerable<Set> sets = null;

                if (!string.IsNullOrWhiteSpace(setName) && !string.IsNullOrWhiteSpace(setCode))
                {
                    var sets1 = await _setsRepository.FindByNameAsync(setName).ConfigureAwait(true);
                    var sets2 = await _setsRepository.FindByCodeAsync(setCode).ConfigureAwait(true);
                    sets = sets1.Intersect(sets2);
                }
                else if (!string.IsNullOrWhiteSpace(setName))
                {
                    sets = await _setsRepository.FindByNameAsync(setName).ConfigureAwait(true);
                }
                else if (!string.IsNullOrWhiteSpace(setCode))
                {
                    sets = await _setsRepository.FindByCodeAsync(setCode).ConfigureAwait(true);
                }
                else
                {
                    sets = await _setsRepository.ListAsync().ConfigureAwait(true);
                }

                sets = (limit != 0) ? sets.Skip(offset).Take(limit) : sets.Skip(offset);

                if (onlyCount)
                {
                    var response = new ObjectResponse<IEnumerable<SetResource>>(null);
                    response.Count = sets.Count();

                    return response;
                }

                var result = _mapper.Map<IEnumerable<Set>, IEnumerable<SetResource>>(sets);

                return new ObjectResponse<IEnumerable<SetResource>>(result);
            }
            catch (Exception ex)
            {
                return new ObjectResponse<IEnumerable<SetResource>>(
                    $"Error occured while searching sets: {ex.Message}", 500);
            }
        }

        public async Task<ObjectResponse<SetResource>> GetSetByIdAsync(long id)
        {
            try
            {
                var set = await _setsRepository.GetByIdAsync(id).ConfigureAwait(true);

                if (set == null)
                {
                    return new ObjectResponse<SetResource>(
                        $"Set with specified id not found", 404);
                }

                var result = _mapper.Map<Set, SetResource>(set);

                return new ObjectResponse<SetResource>(result);
            }
            catch (Exception ex)
            {
                return new ObjectResponse<SetResource>(
                    $"Error occured while getting Set: {ex.Message}", 500);
            }
        }

        public async Task<ObjectResponse<SetResource>> AddSetAsync(SetResource resource)
        {
            try
            {
                var set = _mapper.Map<SetResource, Set>(resource);

                Validate(set);

                await _setsRepository.AddAsync(set).ConfigureAwait(true);
                await _unitOfWork.CompleteAsync().ConfigureAwait(true);

                var result = _mapper.Map<Set, SetResource>(set);

                return new ObjectResponse<SetResource>(result);

            }
            catch (Exception ex) when (ex is ArgumentException || ex is ArgumentNullException)
            {
                return new ObjectResponse<SetResource>(
                    $"Error occured while adding Set: {ex.Message}", 400);
            }
            catch (Exception ex)
            {
                return new ObjectResponse<SetResource>(
                    $"Error occured while adding Set: {ex.Message}", 500);
            }
        }

        public async Task<ObjectResponse<SetResource>> UpdateSetAsync(SetResource resource)
        {
            try
            {
                var set = _mapper.Map<SetResource, Set>(resource);

                Validate(set);

                var temp = await _setsRepository.GetByIdAsync(set.Id).ConfigureAwait(true);
                if (temp == null)
                {
                    return new ObjectResponse<SetResource>(
                        $"Set with specified id not found", 404);
                }

                _setsRepository.Update(set);
                await _unitOfWork.CompleteAsync().ConfigureAwait(true);

                var result = _mapper.Map<Set, SetResource>(set);

                return new ObjectResponse<SetResource>(result);

            }
            catch (Exception ex) when (ex is ArgumentException || ex is ArgumentNullException)
            {
                return new ObjectResponse<SetResource>(
                    $"Error occured while updating Set: {ex.Message}", 400);
            }
            catch (Exception ex)
            {
                return new ObjectResponse<SetResource>(
                    $"Error occured while updating Set: {ex.Message}", 500);
            }
        }

        public async Task<EmptyResponse> DeleteSetByIdAsync(long id)
        {
            try
            {
                var set = await _setsRepository.GetByIdAsync(id).ConfigureAwait(true);
                if (set == null)
                {
                    return new EmptyResponse($"Set with specified id not found", 404);
                }

                _setsRepository.Remove(set);
                await _unitOfWork.CompleteAsync().ConfigureAwait(true);

                return new EmptyResponse();
            }
            catch (Exception ex)
            {
                return new EmptyResponse($"Error occured while deleting Set: {ex.Message}", 500);
            }
        }

        private void Validate(Set set)
        {
            if (set == null)
            {
                throw new ArgumentNullException(nameof(set));
            }

            if (string.IsNullOrWhiteSpace(set.Name))
            {
                throw new ArgumentException("Set name can't be empty.");
            }

            if (set.ReleaseDate == null)
            {
                throw new ArgumentNullException(nameof(set.ReleaseDate));
            }

            if (set.ReleaseDate < new DateTime(1900))
            {
                throw new ArgumentException("Release date is incorrect.");
            }
        }
    }
}
