﻿using AutoMapper;
using CardExchangerServer.Communication;
using CardExchangerServer.Data.Models;
using CardExchangerServer.Data.Resources;
using CardExchangerServer.Interfaces;
using CardExchangerServer.Interfaces.Repositories;
using CardExchangerServer.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CardExchangerServer.Services
{
    public class ExchangesService : IExchangesService
    {
        private readonly IExchangesRepository _exchangesRepository;
        private readonly ICardsRepository _cardsRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ExchangesService(IExchangesRepository exchangesRepository,
                                ICardsRepository cardsRepository,
                                IUnitOfWork unitOfWork,
                                IMapper mapper)
        {
            _exchangesRepository = exchangesRepository;
            _cardsRepository = cardsRepository;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<ObjectResponse<IEnumerable<ExchangeResource>>> GetDialogueExchangesAsync(long dialogueId)
        {
            try
            {
                var exchages = await _exchangesRepository
                    .GetDialogueExchangesAsync(dialogueId).ConfigureAwait(true);

                var result = _mapper
                    .Map<IEnumerable<Exchange>, IEnumerable<ExchangeResource>>(exchages);

                return new ObjectResponse<IEnumerable<ExchangeResource>>(result);
            }
            catch (Exception e)
            {
                return new ObjectResponse<IEnumerable<ExchangeResource>>($"{e.Message}", 500);
            }
        }

        public async Task<ObjectResponse<IEnumerable<ExchangeResource>>> GetUserExchangesAsync(long userId)
        {
            try
            {
                var exchages = await _exchangesRepository
                    .GetUserExchangesAsync(userId).ConfigureAwait(true);

                var result = _mapper
                    .Map<IEnumerable<Exchange>, IEnumerable<ExchangeResource>>(exchages);

                return new ObjectResponse<IEnumerable<ExchangeResource>>(result);
            }
            catch (Exception e)
            {
                return new ObjectResponse<IEnumerable<ExchangeResource>>($"{e.Message}", 500);
            }
        }

        public async Task<ObjectResponse<ExchangeResource>> GetExchangeByIdAsync(long id)
        {
            try
            {
                var exchage = await _exchangesRepository
                    .GetExchangeByIdAsync(id).ConfigureAwait(true);
                if (exchage == null)
                {
                    return new ObjectResponse<ExchangeResource>(
                        "Exchange with specified id not found", 404);
                }

                var result = _mapper.Map<Exchange, ExchangeResource>(exchage);

                return new ObjectResponse<ExchangeResource>(result);
            }
            catch (Exception e)
            {
                return new ObjectResponse<ExchangeResource>($"{e.Message}", 500);
            }
        }

        public async Task<ObjectResponse<ExchangeResource>> AddExchangeAsync(ExchangeResource resource)
        {
            try
            {
                var exchange = _mapper.Map<ExchangeResource, Exchange>(resource);
                ValidateExchage(exchange);

                await _exchangesRepository.AddExchangeAsync(exchange).ConfigureAwait(true);
                await _unitOfWork.CompleteAsync().ConfigureAwait(true);

                var result = _mapper.Map<Exchange, ExchangeResource>(exchange);

                return new ObjectResponse<ExchangeResource>(result);
            }
            catch (Exception e) when (e is ArgumentNullException || e is ArgumentException)
            {
                return new ObjectResponse<ExchangeResource>($"{e.Message}", 400);
            }
            catch (Exception e)
            {
                return new ObjectResponse<ExchangeResource>($"{e.Message}", 500);
            }
        }

        public async Task<ObjectResponse<ExchangeResource>> UpdateExchangeAsync(ExchangeResource resource)
        {
            try
            {
                var exchange = _mapper.Map<ExchangeResource, Exchange>(resource);
                ValidateExchage(exchange);

                var exg = await _exchangesRepository
                    .GetExchangeByIdAsync(exchange.Id).ConfigureAwait(true);
                if (exg == null)
                {
                    return new ObjectResponse<ExchangeResource>(
                        "Exchange with specified id not found", 404);
                }

                _exchangesRepository.UpdateExchange(exchange);
                await _unitOfWork.CompleteAsync().ConfigureAwait(true);

                var result = _mapper.Map<Exchange, ExchangeResource>(exchange);

                return new ObjectResponse<ExchangeResource>(result);
            }
            catch (Exception e) when (e is ArgumentNullException || e is ArgumentException)
            {
                return new ObjectResponse<ExchangeResource>($"{e.Message}", 400);
            }
            catch (Exception e)
            {
                return new ObjectResponse<ExchangeResource>($"{e.Message}", 500);
            }
        }

        public async Task<EmptyResponse> DeleteExchangeAsync(long id)
        {
            try
            {
                var exchange = await _exchangesRepository
                    .GetExchangeByIdAsync(id).ConfigureAwait(true);
                if (exchange == null)
                {
                    return new EmptyResponse("Exchange with specified id not found", 404);
                }

                _exchangesRepository.RemoveExchange(exchange);
                await _unitOfWork.CompleteAsync().ConfigureAwait(true);

                return new EmptyResponse();
            }
            catch (Exception e)
            {
                return new EmptyResponse($"{e.Message}", 500);
            }
        }

        public async Task<ObjectResponse<IEnumerable<ExchangeCardResource>>> GetExchangeCardsAsync(long exchangeId)
        {
            try
            {
                var exchangeCards = await _exchangesRepository
                    .GetExchangeCardsAsync(exchangeId).ConfigureAwait(true);

                var result = _mapper
                    .Map<IEnumerable<ExchangeCard>, IEnumerable<ExchangeCardResource>>(exchangeCards);

                return new ObjectResponse<IEnumerable<ExchangeCardResource>>(result);
            }
            catch (Exception e)
            {
                return new ObjectResponse<IEnumerable<ExchangeCardResource>>($"{e.Message}", 500);
            }
        }

        public async Task<ObjectResponse<IEnumerable<ExchangeCardResource>>> GetUserExchangeCardsAsync(long exchangeId, long userId)
        {
            try
            {
                var exchangeCards = await _exchangesRepository
                    .GetUserExchangeCardsAsync(exchangeId, userId).ConfigureAwait(true);

                var result = _mapper
                    .Map<IEnumerable<ExchangeCard>, IEnumerable<ExchangeCardResource>>(exchangeCards);

                return new ObjectResponse<IEnumerable<ExchangeCardResource>>(result);
            }
            catch (Exception e)
            {
                return new ObjectResponse<IEnumerable<ExchangeCardResource>>($"{e.Message}", 500);
            }
        }

        public async Task<ObjectResponse<ExchangeCardResource>> AddExchangeCardAsync(long exchangeId, ExchangeCardResource resource)
        {
            try
            {
                var exchangeCard = _mapper.Map<ExchangeCardResource, ExchangeCard>(resource);

                var exchange = await _exchangesRepository
                    .GetExchangeByIdAsync(exchangeId).ConfigureAwait(true);
                if (exchange == null)
                {
                    return new ObjectResponse<ExchangeCardResource>(
                        "Exchange with specified id not found", 404);
                }
                else if (exchange.User1Id != exchangeCard.UserId && exchange.User2Id != exchangeCard.UserId)
                {
                    return new ObjectResponse<ExchangeCardResource>(
                        "User with specified id does not take part in exchange", 400);
                }

                var card = await _cardsRepository.GetByIdAsync(exchangeCard.CardId).ConfigureAwait(true);
                if (card == null)
                {
                    return new ObjectResponse<ExchangeCardResource>(
                        "Card with specified id not found", 404);
                }

                exchangeCard.ExchangeId = exchangeId;

                await _exchangesRepository
                    .AddExchangeCardAsync(exchangeCard).ConfigureAwait(true);
                await _unitOfWork.CompleteAsync().ConfigureAwait(true);

                var result = _mapper.Map<ExchangeCard, ExchangeCardResource>(exchangeCard);

                return new ObjectResponse<ExchangeCardResource>(result);
            }
            catch (Exception e)
            {
                return new ObjectResponse<ExchangeCardResource>($"{e.Message}", 500);
            }
        }

        public async Task<ObjectResponse<ExchangeCardResource>> UpdateExchangeCardAsync(long exchangeId, ExchangeCardResource resource)
        {
            try
            {
                var exchangeCard = _mapper.Map<ExchangeCardResource, ExchangeCard>(resource);

                var exchange = await _exchangesRepository
                    .GetExchangeByIdAsync(exchangeId).ConfigureAwait(true);
                if (exchange == null)
                {
                    return new ObjectResponse<ExchangeCardResource>(
                        "Exchange with specified id not found", 404);
                }
                else if (exchange.User1Id != exchangeCard.UserId &&
                         exchange.User2Id != exchangeCard.UserId)
                {
                    return new ObjectResponse<ExchangeCardResource>(
                        "User with specified id does not take part in exchange", 400);
                }

                var card = await _cardsRepository
                    .GetByIdAsync(exchangeCard.CardId).ConfigureAwait(true);
                if (card == null)
                {
                    return new ObjectResponse<ExchangeCardResource>(
                        "Card with specified id not found", 404);
                }

                exchangeCard.ExchangeId = exchangeId;

                _exchangesRepository.UpdateExchangeCard(exchangeCard);
                await _unitOfWork.CompleteAsync().ConfigureAwait(true);

                var result = _mapper.Map<ExchangeCard, ExchangeCardResource>(exchangeCard);

                return new ObjectResponse<ExchangeCardResource>(result);
            }
            catch (Exception e)
            {
                return new ObjectResponse<ExchangeCardResource>($"{e.Message}", 500);
            }
        }

        public async Task<EmptyResponse> DeleteExchangeCardAsync(long exchangeId, ExchangeCardResource resource)
        {
            try
            {
                var exchangeCard = _mapper.Map<ExchangeCardResource, ExchangeCard>(resource);

                var exchange = await _exchangesRepository
                    .GetExchangeByIdAsync(exchangeId).ConfigureAwait(true);
                if (exchange == null)
                {
                    return new EmptyResponse(
                        "Exchange with specified id not found", 404);
                }
                else if (exchange.User1Id != exchangeCard.UserId &&
                         exchange.User2Id != exchangeCard.UserId)
                {
                    return new EmptyResponse(
                        "User with specified id does not take part in exchange", 400);
                }

                exchangeCard.ExchangeId = exchangeId;

                var ec = await _exchangesRepository
                    .GetExchangeCardByValueAsync(exchangeCard).ConfigureAwait(true);
                if (exchange == null)
                {
                    return new EmptyResponse(
                        "Card with specified id and user not found in current exchange", 404);
                }

                _exchangesRepository.RemoveExchangeCard(exchangeCard);
                await _unitOfWork.CompleteAsync().ConfigureAwait(true);

                return new EmptyResponse();
            }
            catch (Exception e)
            {
                return new EmptyResponse($"{e.Message}", 500);
            }
        }

        private void ValidateExchage(Exchange exchange)
        {
            if (exchange == null)
            {
                throw new ArgumentNullException(nameof(exchange));
            }

            if (exchange.CreationTime > DateTime.Now)
            {
                throw new ArgumentException("Creation time can't be greater than current time.");
            }
        }

    }
}
