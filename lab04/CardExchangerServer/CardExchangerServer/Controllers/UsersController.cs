﻿using CardExchangerServer.Data.Resources;
using CardExchangerServer.Extensions;
using CardExchangerServer.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CardExchangerServer.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUsersService _usersService;
        private readonly IDialoguesService _dialoguesService;
        private readonly ICardsService _cardsService;
        private readonly IExchangesService _exchangesService;

        public UsersController(IUsersService service,
                               IDialoguesService dialoguesService,
                               ICardsService cardsService,
                               IExchangesService exchangesService)
        {
            _usersService = service;
            _dialoguesService = dialoguesService;
            _cardsService = cardsService;
            _exchangesService = exchangesService;
        }

        [HttpPost("singin")]
        public virtual async Task<IActionResult> Singin([FromBody] InUserResource resource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrorMessages());
            }

            var response = await _usersService.SinginAsync(resource).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return new ObjectResult(response.Object);
        }

        [HttpGet("login")]
        public virtual async Task<IActionResult> Login([FromQuery] string login, [FromQuery] string password)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrorMessages());
            }

            var response = await _usersService
                .LoginAsync(new InUserResource { Login = login, Password = password }).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return new ObjectResult(response.Object);
        }

        [HttpGet]
        //[Authorize(Roles = "Admin")]
        public virtual async Task<IActionResult> GetAllUsersAsync([FromQuery]string? login)
        {
            var response = await _usersService.GetUserListAsync(login).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return new ObjectResult(response.Object);
        }

        [HttpPut]
        //[Authorize(Roles = "Admin")]
        public virtual async Task<IActionResult> UpdateUserAsync([FromBody]InUserResource resource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrorMessages());
            }

            var response = await _usersService.UpdateUserAsync(resource).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return new ObjectResult(response.Object);
        }

        [HttpGet("{userId}")]
        //[Authorize(Roles = "Admin")]
        public virtual async Task<IActionResult> GetUserByIdAsync([FromRoute]long userId)
        {
            var response = await _usersService.GetUserByIdAsync(userId).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return new ObjectResult(response.Object);
        }

        [HttpDelete("{userId}")]
        //[Authorize(Roles = "Admin")]
        public virtual async Task<IActionResult> DeleteUserByIdAsync([FromRoute]long userId)
        {
            var response = await _usersService.DeleteUserByIdAsync(userId).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return Ok();
        }

        [HttpGet("{userId}/cards")]
        //[Authorize(Roles = "User")]
        public virtual async Task<IActionResult> GetUserCardsAsync([FromRoute]long userId,
                                                                   [FromQuery]int offset,
                                                                   [FromQuery]int limit)
        {
            var response = await _cardsService.GetUserCardsAsync(userId, offset, limit).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return new ObjectResult(response.Object);
        }

        [HttpPost("{userId}/cards")]
        //[Authorize(Roles = "User")]
        public virtual async Task<IActionResult> AddUserCardAsync([FromRoute]long userId,
                                                                  [FromBody]CollectionResource resource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrorMessages());
            }

            var response = await _cardsService.AddUserCardAsync(userId, resource).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return new ObjectResult(response.Object);
        }

        [HttpDelete("{userId}/cards")]
        //[Authorize(Roles = "User")]
        public virtual async Task<IActionResult> DeleteUserCardAsync([FromRoute]long userId,
                                                                     [FromBody]CollectionResource resource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrorMessages());
            }

            var response = await _cardsService.DeleteUserCardAsync(userId, resource).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return Ok();
        }

        [HttpGet("{userId}/dialogues")]
        //[Authorize(Roles = "User")]
        public async Task<IActionResult> GetUserDialoguesAsync([FromRoute]long userId)
        {
            var response = await _dialoguesService.GetUserDialoguesAsync(userId).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return new ObjectResult(response.Object);
        }

        [HttpGet("{userId}/exchanges")]
        //[Authorize(Roles = "User")]
        public async Task<IActionResult> GetUserExchangesAsync([FromRoute]long userId)
        {
            var response = await _exchangesService.GetUserExchangesAsync(userId).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return new ObjectResult(response.Object);
        }
    }
}
