﻿using CardExchangerServer.Data.Resources;
using CardExchangerServer.Extensions;
using CardExchangerServer.Hubs;
using CardExchangerServer.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace CardExchangerServer.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class ExchangesController : ControllerBase
    {
        private readonly IExchangesService _exchangesService;
        private readonly IHubContext<ChatHub> _hubContext;

        public ExchangesController(IExchangesService exchangesService, IHubContext<ChatHub> hubContext)
        {
            _exchangesService = exchangesService;
            _hubContext = hubContext;
        }

        [HttpGet("{exchangeId}")]
        public async Task<IActionResult> GetExchangeByIdAsync([FromRoute]long exchangeId)
        {
            var response = await _exchangesService.GetExchangeByIdAsync(exchangeId).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return new ObjectResult(response.Object);
        }

        [HttpPost]
        public async Task<IActionResult> AddExchangeAsync([FromBody]ExchangeResource resource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrorMessages());
            }

            var response = await _exchangesService.AddExchangeAsync(resource).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return new ObjectResult(response.Object);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateExchangeAsync([FromBody]ExchangeResource resource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrorMessages());
            }

            var response = await _exchangesService.UpdateExchangeAsync(resource).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return new ObjectResult(response.Object);
        }

        [HttpDelete("{exchangeId}")]
        public async Task<IActionResult> DeleteExchangeAsync([FromRoute]long exchangeId)
        {
            var response = await _exchangesService.DeleteExchangeAsync(exchangeId).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return Ok();
        }

        [HttpGet("{exchangeId}/cards")]
        public async Task<IActionResult> GetExchangeCardsAsync([FromRoute]long exchangeId, [FromQuery]long userId)
        {
            var response = await _exchangesService.GetUserExchangeCardsAsync(exchangeId, userId).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return new ObjectResult(response.Object);
        }

        [HttpPost("{exchangeId}/cards")]
        public async Task<IActionResult> AddExchangeCardAsync([FromRoute]long exchangeId,
                                                              [FromBody]ExchangeCardResource resource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrorMessages());
            }

            var response = await _exchangesService.AddExchangeCardAsync(exchangeId, resource).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return new ObjectResult(response.Object);
        }

        [HttpPut("{exchangeId}/cards")]
        public async Task<IActionResult> UpdateExchangeCardAsync([FromRoute]long exchangeId,
                                                                 [FromBody]ExchangeCardResource resource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrorMessages());
            }

            var response = await _exchangesService.UpdateExchangeCardAsync(exchangeId, resource).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return new ObjectResult(response.Object);
        }

        [HttpDelete("{exchangeId}/cards")]
        public async Task<IActionResult> DeleteExchangeCardAsync([FromRoute]long exchangeId,
                                                                 [FromBody]ExchangeCardResource resource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrorMessages());
            }

            var response = await _exchangesService.DeleteExchangeCardAsync(exchangeId, resource).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return Ok();
        }
    }
}
