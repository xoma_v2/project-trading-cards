﻿using CardExchangerServer.Data.Resources;
using CardExchangerServer.Extensions;
using CardExchangerServer.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CardExchangerServer.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CardsController : ControllerBase
    {
        private readonly ICardsService _cardsService;


        public CardsController(ICardsService cardsService)
        {
            _cardsService = cardsService;
        }

        [HttpGet]
        public async Task<IActionResult>
            FindCardsAsync([FromQuery]string? cardText,
                           [FromQuery]string? setCode,
                           [FromQuery]string? cardType,
                           [FromQuery]int offset,
                           [FromQuery]int limit,
                           [FromQuery]bool onlyCount)
        {
            var response = await _cardsService
                .FindCardsAsync(cardText, setCode, cardType, offset, limit, onlyCount)
                .ConfigureAwait(true);

            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            if (onlyCount)
            {
                return new ObjectResult(new { Count = response.Count });
            }

            return new ObjectResult(response.Object);
        }

        [HttpPost]
        //[Authorize(Roles = "Admin")]
        public async Task<IActionResult> AddCardAsync([FromBody]CardResource resource)
        {
            if (!ModelState.IsValid)
            {
                return StatusCode(400, ModelState.GetErrorMessages());
            }

            var response = await _cardsService.AddCardAsync(resource).ConfigureAwait(true);

            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return new ObjectResult(response.Object);
        }

        [HttpPut]
        //[Authorize(Roles = "Admin")]
        public async Task<IActionResult> UpdateCardAsync([FromBody]CardResource resource)
        {
            if (!ModelState.IsValid)
            {
                return StatusCode(400, ModelState.GetErrorMessages());
            }

            var response = await _cardsService.UpdateCardAsync(resource).ConfigureAwait(true);

            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return new ObjectResult(response.Object);
        }

        [HttpGet("{cardId}")]
        public async Task<IActionResult> GetCardByIdAsync([FromRoute]long cardId)
        {
            var response = await _cardsService.GetCardByIdAsync(cardId).ConfigureAwait(true);

            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return new ObjectResult(response.Object);
        }

        [HttpDelete("{cardId}")]
        //[Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteCardByIdAsync([FromRoute]long cardId)
        {
            var response = await _cardsService.DeleteCardByIdAsync(cardId).ConfigureAwait(true);

            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return Ok();
        }
    }
}
