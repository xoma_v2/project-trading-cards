﻿using CardExchangerServer.Data.Resources;
using CardExchangerServer.Extensions;
using CardExchangerServer.Hubs;
using CardExchangerServer.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace CardExchangerServer.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class DialoguesController : ControllerBase
    {
        private readonly IDialoguesService _dialoguesService;
        private readonly IExchangesService _exchangesService;
        private readonly IHubContext<ChatHub> _hubContext;

        public DialoguesController(IDialoguesService dialoguesService,
                                   IExchangesService exchangesService,
                                   IHubContext<ChatHub> hubContext)
        {
            _dialoguesService = dialoguesService;
            _exchangesService = exchangesService;
            _hubContext = hubContext;
        }

        [HttpGet("{dialogueId}")]
        public async Task<IActionResult> GetDialogueByIdAsync([FromRoute]long dialogueId)
        {
            var response = await _dialoguesService.GetDialogueByIdAsync(dialogueId).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return new ObjectResult(response.Object);
        }

        [HttpPost]
        public async Task<IActionResult> AddDialogueAsync([FromBody]DialogueResource resource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrorMessages());
            }

            var response = await _dialoguesService.AddDialogueAsync(resource).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return new ObjectResult(response.Object);
        }

        [HttpDelete("{dialogueId}")]
        public async Task<IActionResult> DeleteDialogueAsync([FromRoute]long dialogueId)
        {
            var response = await _dialoguesService.DeleteDialogueAsync(dialogueId).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return Ok();
        }

        [HttpGet("{dialogueId}/messages")]
        public async Task<IActionResult> GetDialogueMessagesAsync([FromRoute]long dialogueId)
        {
            var response = await _dialoguesService.GetDialogueMessagesAsync(dialogueId).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return new ObjectResult(response.Object);
        }

        [HttpGet("{dialogueId}/messages/last")]
        public async Task<IActionResult> GetLastDialogueMessageAsync([FromRoute]long dialogueId)
        {
            var response = await _dialoguesService.GetLastDialogueMessageAsync(dialogueId).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return new ObjectResult(response.Object);
        }

        [HttpPost("{dialogueId}/messages")]
        public async Task<IActionResult> AddMessageAsync([FromRoute]long dialogueId, [FromBody]MessageResource resource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrorMessages());
            }

            var response = await _dialoguesService.AddMessageAsync(dialogueId, resource).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return new ObjectResult(response.Object);
        }

        [HttpPut("{dialogueId}/messages")]
        public async Task<IActionResult> UpdateMessageAsync([FromRoute]long dialogueId, [FromBody]MessageResource resource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrorMessages());
            }

            var response = await _dialoguesService.UpdateMessageAsync(dialogueId, resource).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return new ObjectResult(response.Object);
        }

        [HttpDelete("{dialogueId}/messages/{messageId}")]
        public async Task<IActionResult> DeleteMessageAsync([FromRoute]long dialogueId, [FromRoute]long messageId)
        {
            var response = await _dialoguesService.DeleteMessageAsync(dialogueId, messageId).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return Ok();
        }

        [HttpGet("{dialogueId}/exchanges")]
        public async Task<IActionResult> GetDialogueExchangesAsync([FromRoute]long dialogueId)
        {
            var response = await _exchangesService.GetDialogueExchangesAsync(dialogueId).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return new ObjectResult(response.Object);
        }
    }
}
