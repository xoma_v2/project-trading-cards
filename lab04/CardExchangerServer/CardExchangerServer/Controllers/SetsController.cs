﻿using CardExchangerServer.Data.Resources;
using CardExchangerServer.Extensions;
using CardExchangerServer.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CardExchangerServer.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SetsController : ControllerBase
    {
        private readonly ISetsService _setsService;
        private readonly ICardsService _cardsService;

        public SetsController(ISetsService setsService, ICardsService cardsService)
        {
            _setsService = setsService;
            _cardsService = cardsService;
        }

        [HttpGet]
        public async Task<IActionResult> FindSetsAsync([FromQuery]string? setName,
                                                       [FromQuery]string? setCode,
                                                       [FromQuery]int offset,
                                                       [FromQuery]int limit,
                                                       [FromQuery]bool onlyCount)
        {
            var response = await _setsService
                .FindSetsAsync(setName, setCode, offset, limit, onlyCount).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            if (onlyCount)
            {
                return new ObjectResult(new { Count = response.Count });
            }

            return new ObjectResult(response.Object);
        }

        [HttpPost]
        //[Authorize(Roles = "Admin")]
        public async Task<IActionResult> AddSetAsync([FromBody]SetResource resource)
        {
            if (!ModelState.IsValid)
            {
                return StatusCode(400, ModelState.GetErrorMessages());
            }

            var response = await _setsService.AddSetAsync(resource).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return new ObjectResult(response.Object);
        }

        [HttpPut]
        //[Authorize(Roles = "Admin")]
        public async Task<IActionResult> UpdateSetAsync([FromBody]SetResource resource)
        {
            if (!ModelState.IsValid)
            {
                return StatusCode(400, ModelState.GetErrorMessages());
            }

            var response = await _setsService.UpdateSetAsync(resource).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return new ObjectResult(response.Object);
        }

        [HttpGet("{setId}")]
        public async Task<IActionResult> GetSetByIdAsync([FromRoute]long setId)
        {
            var response = await _setsService.GetSetByIdAsync(setId).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return new ObjectResult(response.Object);
        }

        [HttpDelete("{setId}")]
        //[Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteSetByIdAsync([FromRoute]long setId)
        {
            var response = await _setsService.DeleteSetByIdAsync(setId).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return Ok();
        }

        [HttpGet("{setId}/cards")]
        public async Task<IActionResult> GetSetCardsAsync([FromRoute]long setId)
        {
            var response = await _cardsService.GetSetCardListAsync(setId).ConfigureAwait(true);
            if (!response.Success)
            {
                return StatusCode(response.StatusCode, response.Message);
            }

            return new ObjectResult(response.Object);
        }
    }
}