﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace CardExchangerServer.Controllers
{
    public static class AuthOptions
    {
        public const string ISSUER = "Server";
        public const string AUDIENCE = "https://localhost:5000/";
        const string KEY = "(=^:^=)mysupersecret_secretkey(=^:^=)";
        public const int JWT_TOKEN_LIFETIME = 10;
        public const int RT_TOKEN_LIFETIME = 1000;

        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
