using AutoMapper;
using CardExchangerServer.Controllers;
using CardExchangerServer.Data;
using CardExchangerServer.Hubs;
using CardExchangerServer.Interfaces;
using CardExchangerServer.Interfaces.Repositories;
using CardExchangerServer.Interfaces.Services;
using CardExchangerServer.Repositories;
using CardExchangerServer.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Connections;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CardExchangerServer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy", builder => builder
                    .WithOrigins(Configuration["ClientUrl"])
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });

            services.AddDbContext<CardExchangerContext>(options => options.UseNpgsql(Configuration["ConnectionString:DefaultConnection"]));
            services.AddControllers();

            services.AddScoped<ICardsRepository, CardsRepository>();
            services.AddScoped<ISetsRepository, SetsRepository>();
            services.AddScoped<IUsersRepository, UsersRepository>();
            services.AddScoped<IDialoguesRepository, DialoguesRepository>();
            services.AddScoped<IExchangesRepository, ExchangesRepository>();

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddScoped<ICardsService, CardsService>();
            services.AddScoped<ISetsService, SetsService>();
            services.AddScoped<IUsersService, UsersService>();
            services.AddScoped<IDialoguesService, DialoguesService>();
            services.AddScoped<IExchangesService, ExchangesService>();

            services.AddSingleton<IUserIdProvider, CustomUserIdProvider>();

            services.AddAutoMapper(typeof(Startup));

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidIssuer = AuthOptions.ISSUER,
                        ValidateAudience = true,
                        ValidAudience = AuthOptions.AUDIENCE,
                        ValidateLifetime = true,
                        IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),
                        ValidateIssuerSigningKey = true,
                        RoleClaimType = ClaimsIdentity.DefaultRoleClaimType
                    };
                    options.Events = new JwtBearerEvents
                    {
                        OnMessageReceived = context =>
                        {
                            var accessToken = context.Request.Query["access_token"];

                            var path = context.HttpContext.Request.Path;
                            if (!string.IsNullOrEmpty(accessToken) &&
                                (path.StartsWithSegments("/chat")))
                            {
                                context.Token = accessToken;
                            }
                            return Task.CompletedTask;
                        }
                    };
                });

            services.AddSignalR().AddHubOptions<ChatHub>(options =>
            {
                options.EnableDetailedErrors = true;
                options.KeepAliveInterval = System.TimeSpan.FromDays(1);
                options.ClientTimeoutInterval = System.TimeSpan.FromDays(1);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //app.UseHttpsRedirection();
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseRouting();

            app.UseCors("CorsPolicy");

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<ChatHub>("/chat", options =>
                {
                    options.ApplicationMaxBufferSize = 64;
                    options.TransportMaxBufferSize = 64;
                    options.LongPolling.PollTimeout = System.TimeSpan.FromMinutes(1);
                    //options.Transports = HttpTransportType.LongPolling | HttpTransportType.WebSockets;
                });
            });
        }
    }
}
