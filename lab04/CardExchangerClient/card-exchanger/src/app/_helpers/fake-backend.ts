import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';

import { User } from '../_models/user';
import { Card } from '../_models/card';
import { Set } from '../_models/set';
import { Dialogue } from '../_models/dialogue';
import { Message } from '../_models/message';

const users: User[] = [
    { id: 1, login: 'admin', password: 'admin', name: 'Admin', role: User.Role.Admin },
    { id: 2, login: 'user', password: 'user', name: 'Normal', role: User.Role.User },
    { id: 3, login: 'nikita', password: 'qwerty', name: 'Nikita', role: User.Role.Admin },
    { id: 4, login: 'alex', password: '123', name: 'Alex', role: User.Role.Admin },

    { id: 5, login: 'user5', password: 'qwerty123', name: 'User5', role: User.Role.User },
    { id: 6, login: 'user6', password: 'qwerty123', name: 'User6', role: User.Role.User },
    { id: 7, login: 'user7', password: 'qwerty123', name: 'User7', role: User.Role.User },
    { id: 8, login: 'user8', password: 'qwerty123', name: 'User8', role: User.Role.User },
    { id: 9, login: 'user9', password: 'qwerty123', name: 'User9', role: User.Role.User },
    { id: 10, login: 'user10', password: 'qwerty123', name: 'User10', role: User.Role.User },
    { id: 11, login: 'user11', password: 'qwerty123', name: 'User11', role: User.Role.User },
    { id: 12, login: 'user12', password: 'qwerty123', name: 'User12', role: User.Role.User },
    { id: 13, login: 'user13', password: 'qwerty123', name: 'User13', role: User.Role.User },
    { id: 14, login: 'user14', password: 'qwerty123', name: 'User14', role: User.Role.User },
    { id: 15, login: 'user15', password: 'qwerty123', name: 'User15', role: User.Role.User }
];

const dialogues: Dialogue[] = [
    { id: 1, user1Id: 1, user2Id: 6, creationTime: new Date('2020-01-13') },
    { id: 2, user1Id: 9, user2Id: 1, creationTime: new Date('2020-01-17') },
    { id: 3, user1Id: 5, user2Id: 1, creationTime: new Date('2020-01-27') },
    { id: 4, user1Id: 1, user2Id: 13, creationTime: new Date('2020-01-28') },
    { id: 5, user1Id: 1, user2Id: 3, creationTime: new Date('2020-01-01') },
    { id: 6, user1Id: 1, user2Id: 10, creationTime: new Date('2020-01-13') },
    { id: 7, user1Id: 4, user2Id: 1, creationTime: new Date('2020-01-18') },
    { id: 8, user1Id: 2, user2Id: 11, creationTime: new Date('2020-01-13') },
    { id: 9, user1Id: 10, user2Id: 2, creationTime: new Date('2020-01-29') },
    { id: 10, user1Id: 2, user2Id: 6, creationTime: new Date('2020-01-5') },
];

const messages: Message[] = [
    { id: 1, dialogueId: 1, text: 'Какое-то безумно интересное сообщение1((((', time: new Date('2020-01-13'), userId: 1 },
    { id: 2, dialogueId: 2, text: 'Какое-то безумно интересное сообщение2((((', time: new Date('2020-01-17'), userId: 9 },
    { id: 3, dialogueId: 3, text: 'Какое-то безумно интересное сообщение3((((', time: new Date('2020-01-27'), userId: 5 },
    { id: 4, dialogueId: 4, text: 'Какое-то безумно интересное сообщение4((((', time: new Date('2020-01-28'), userId: 1 },
    { id: 5, dialogueId: 5, text: 'Какое-то безумно интересное сообщение5((((', time: new Date('2020-01-01'), userId: 1 },
    { id: 6, dialogueId: 6, text: 'Какое-то безумно интересное сообщение6((((', time: new Date('2020-01-13'), userId: 1 },
    { id: 7, dialogueId: 7, text: 'Какое-то безумно интересное сообщение7((((', time: new Date('2020-01-18'), userId: 4 },
    { id: 8, dialogueId: 8, text: 'Какое-то безумно интересное сообщение8((((', time: new Date('2020-01-13'), userId: 2 },
    { id: 9, dialogueId: 9, text: 'Какое-то безумно интересное сообщение9((((', time: new Date('2020-01-29'), userId: 10 },
    { id: 10, dialogueId: 10, text: 'Какое-то безумно интересное сообщение10((((', time: new Date('2020-01-5'), userId: 2 }
];

const cards: Card[] = [
    {
        id: 130483,
        name: "Abundance",
        description: "If you would draw a card, you may instead choose land or nonland and reveal cards from the top of your library until you reveal a card of the chosen kind. Put that card into your hand and put all other cards revealed this way on the bottom of your library in any order.",
        image: "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=130483&type=card",
        mana: "{2}{G}{G}",
        price: -1,
        setCode: "10E",
        types: []
    },
    {
        id: 132072,
        name: "Academy Researchers",
        description: "When Academy Researchers enters the battlefield, you may put an Aura card from your hand onto the battlefield attached to Academy Researchers.",
        image: "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=132072&type=card",
        mana: "{1}{U}{U}",
        price: -1,
        setCode: "10E",
        types: []
    },
    {
        id: 129458,
        name: "Adarkar Wastes",
        description: "{T}: Add {C}.\n{T}: Add {W} or {U}. Adarkar Wastes deals 1 damage to you.",
        image: "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=129458&type=card",
        price: -1,
        setCode: "10E",
        types: []
    },
    {
        id: 135206,
        name: "Afflict",
        description: "Target creature gets -1/-1 until end of turn.\nDraw a card.",
        image: "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=135206&type=card",
        mana: "{2}{B}",
        price: -1,
        setCode: "10E",
        types: []
    },
    {
        id: 130525,
        name: "Aggressive Urge",
        description: "Target creature gets +1/+1 until end of turn.\nDraw a card.",
        image: "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=130525&type=card",
        mana: "{1}{G}",
        price: -1,
        setCode: "10E",
        types: []
    },
    {
        id: 135228,
        name: "Agonizing Memories",
        description: "Look at target player's hand and choose two cards from it. Put them on top of that player's library in any order.",
        image: "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=135228&type=card",
        mana: "{2}{B}{B}",
        price: -1,
        setCode: "10E",
        types: []
    },
    {
        id: 129459,
        name: "Air Elemental",
        description: "Flying",
        image: "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=129459&type=card",
        mana: "{3}{U}{U}",
        price: -1,
        setCode: "10E",
        types: []
    },
    {
        id: 129913,
        name: "Ambassador Laquatus",
        description: "{3}: Target player puts the top three cards of their library into their graveyard.",
        image: "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=129913&type=card",
        mana: "{1}{U}{U}",
        price: -1,
        setCode: "10E",
        types: []
    },
    {
        id: 134753,
        name: "Anaba Bodyguard",
        description: "First strike (This creature deals combat damage before creatures without first strike.)",
        image: "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=134753&type=card",
        mana: "{3}{R}",
        price: -1,
        setCode: "10E",
        types: []
    }
];

const sets: Set[] = [
    {
        id: 0,
        name: "Tenth Edition",
        releaseDate: new Date("2007-07-13"),
        code: "10E"
    },
    {
        id: 1,
        name: "Revised Edition",
        releaseDate: new Date("1994-04-01"),
        code: "3ED"
    },
    {
        id: 2,
        name: "Fourth Edition Foreign Black Border",
        releaseDate: new Date("1995-04-01"),
        code: "4BB"
    },
    {
        id: 3,
        name: "Fourth Edition",
        releaseDate: new Date("1995-04-01"),
        code: "4ED"
    },
    {
        id: 4,
        name: "Fifth Dawn",
        releaseDate: new Date("2004-06-04"),
        code: "5DN"
    },
    {
        id: 5,
        name: "Fifth Edition",
        releaseDate: new Date("1997-03-24"),
        code: "5ED"
    },
    {
        id: 6,
        name: "Classic Sixth Edition",
        releaseDate: new Date("1999-04-21"),
        code: "6ED"
    },
    {
        id: 7,
        name: "Seventh Edition",
        releaseDate: new Date("2001-04-11"),
        code: "7ED"
    },
    {
        id: 8,
        name: "Eighth Edition",
        releaseDate: new Date("2003-07-28"),
        code: "8ED"
    },
    {
        id: 9,
        name: "Ninth Edition",
        releaseDate: new Date("2005-07-29"),
        code: "9ED"
    },
    {
        id: 10,
        name: "Masters 25",
        releaseDate: new Date("2018-03-16"),
        code: "A25"
    },
];

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const { url, method, headers, body, params } = request;

        // Wrap in delayed observable to simulate server api call.
        return of(null)
            .pipe(mergeMap(handleRoute))
            .pipe(materialize()) // Call materialize and dematerialize to ensure delay even if an error is thrown (https://github.com/Reactive-Extensions/RxJS/issues/648).
            .pipe(delay(500))
            .pipe(dematerialize());

        function handleRoute() {
            //console.log(`${url} ${url.endsWith('/sets')} ${url.match(/\/sets\/\d+$/)} ${url.match(/\/sets\/\d+\/cards$/)} ${idFromUrl(-2)}`);
            switch (true) {
                case url.endsWith('api\/users\/login') && params.get('login') && params.get('password') && method === 'GET':
                    return authenticate(params.get('login'), params.get('password'));

                case url.endsWith('api/users') && method === 'GET':
                    return getUsers();
                case url.match(/api\/users\/\d+$/) && method === 'GET':
                    return getUserById();

                case url.endsWith('api/cards') && method === 'GET':
                    return getCards();
                case url.match(/api\/cards\/\d+$/) && method === 'GET':
                    return getCardById();

                case url.endsWith('api/sets') && method === 'GET':
                    return getSets();
                case url.match(/api\/sets\/\d+$/) && method === 'GET':
                    return getSetById();
                case url.match(/api\/sets\/\d+\/cards$/) && method === 'GET':
                    return getCardsBySetId();

                case url.match(/api\/users\/\d+\/dialogues$/) && method === 'GET':
                    return getDialogues(idFromUrl(-2));
                case url.match(/api\/dialogues\/\d+\/messages$/) && method === 'GET':
                    return getMessages();

                default:
                    // Pass through any requests not handled above.
                    return next.handle(request);
            }
        }

        // ===================================
        // Route functions.
        // ===================================

        function authenticate(login: string, password: string) {
            //const { login, password } = body;
            const user = users.find(x => x.login === login && x.password === password);

            if (!user)
                return error('Username or password is incorrect');

            return ok({
                id: user.id,
                login: user.login,
                name: user.name,
                role: user.role,
                token: `fake-jwt-token.${user.id}`
            });
        }

        function getUsers(): Observable<HttpResponse<User[]>> {
            if (!isAdmin())
                return unauthorized();

            return ok(users);
        }

        function getUserById(): Observable<HttpResponse<User>> {
            if (!isLoggedIn())
                return unauthorized();

            // Only admins can access other user records.
            if (!isAdmin() && currentUser().id !== idFromUrl())
                return unauthorized();

            const user = users.find(x => x.id === idFromUrl());

            return ok(user);
        }

        function getCards(): Observable<HttpResponse<Card[]>> {
            return ok(cards);
        }

        function getCardById(): Observable<HttpResponse<Card>> {
            const card = cards.find(x => x.id === idFromUrl());

            return ok(card);
        }

        function getSets(): Observable<HttpResponse<Set[]>> {
            return ok(sets);
        }

        function getSetById(): Observable<HttpResponse<Set>> {
            const set = sets.find(x => x.id === idFromUrl());

            return ok(set);
        }

        function getCardsBySetId(): Observable<HttpResponse<Card[]>> {
            const set = sets.find(x => x.id === idFromUrl(-2));
            const cardsFromSet = cards.filter((card, index, array) => card.setCode === set.code);

            return ok(cardsFromSet);
        }

        function getDialogues(userId: number): Observable<HttpResponse<Dialogue[]>> {
            if (!isLoggedIn())
                return unauthorized();

            const dialoguesWithCurrentUser = dialogues.filter((dialogue, index, array) =>
                dialogue.user1Id === userId || dialogue.user2Id === userId);

            return ok(dialoguesWithCurrentUser);
        }

        function getMessages(): Observable<HttpResponse<Message[]>> {
            if (!isLoggedIn())
                return unauthorized();

            const messagesFromDialogue = messages.filter((message, index, array) =>
                message.dialogueId === idFromUrl(-2));

            return ok(messagesFromDialogue);
        }

        // ===================================
        // Helper functions.
        // ===================================

        function ok<T>(body: T): Observable<HttpResponse<T>> {
            return of(new HttpResponse({ status: 200, body }));
        }

        function unauthorized() {
            return throwError({ status: 401, error: { message: 'unauthorized' } });
        }

        function error(message) {
            return throwError({ status: 400, error: { message } });
        }

        function isLoggedIn() {
            const authHeader = headers.get('Authorization') || '';

            return authHeader.startsWith('Bearer fake-jwt-token');
        }

        function isAdmin() {
            return isLoggedIn() && currentUser().role === User.Role.Admin;
        }

        function currentUser() {
            if (!isLoggedIn())
                return;

            const id = parseInt(headers.get('Authorization').split('.')[1]);

            return users.find(x => x.id === id);
        }

        // Отрицательная индексация как в питоне.
        function idFromUrl(index: number = -1) {
            const urlParts = url.split('/');

            return parseInt(urlParts[urlParts.length + index]);
        }
    }
}

export const fakeBackendProvider = {
    // Use fake backend in place of Http service for backend-less development.
    provide: HTTP_INTERCEPTORS,
    useClass: FakeBackendInterceptor,
    multi: true
};