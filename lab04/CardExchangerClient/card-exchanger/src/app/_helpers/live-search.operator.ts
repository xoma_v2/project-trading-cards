import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

export function liveSearch<T, R>(
  serviceDelegate: (serviceParam: T) => Observable<R>,
  delay = 200
) {
  return (source$: Observable<T>) => source$.pipe(
    debounceTime(delay),
    distinctUntilChanged(),
    switchMap(serviceDelegate)
  );
}