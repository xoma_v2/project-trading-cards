import { Component, OnInit } from '@angular/core';
import { Observable, combineLatest } from 'rxjs';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { distinctUntilChanged, startWith, tap, debounceTime } from 'rxjs/operators';

import { CollectionService } from '../_services/collection.service';
import { Card } from '../_models/card';
import { liveSearch } from '../_helpers/live-search.operator';
import { AuthenticationService } from '../_services/authentication.service';

@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.css']
})
export class CollectionComponent implements OnInit {
  filterForm: FormGroup;
  offset$: Observable<number>;
  filteredCollection$: Observable<Card[]>;
  get f() { return this.filterForm.controls; }

  currentPage: number;
  limit = 24;

  collectionMode = false;
  get showButtons(): boolean {
    return this.authenticationService.currentUserValue && this.collectionMode;
  }

  constructor(
    private formBuilder: FormBuilder,
    private collectionService: CollectionService,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
    let initParams = this.route.snapshot.queryParams;
    let page = Math.floor(+initParams['page']);
    this.currentPage = page > 0 ? page : 1;
    this.route.paramMap.subscribe(
      paramMap => this.collectionMode = paramMap.get('collectionMode') === 'true'
    );

    this.filterForm = this.formBuilder.group({
      offset: null
    });

    // Чтобы сработал оператор combineLatest() из filteredCollection$, все
    // контролы формы должны быть заполнены какими-то значениями. Для
    // этого испоьзуем оператор startWith(). Почему проброс через метод
    // setValue() ниже не работает - хз.
    this.offset$ = this.f.offset.valueChanges.pipe(
      startWith((this.currentPage - 1) * this.limit),
      distinctUntilChanged()
    );
    this.filteredCollection$ = combineLatest(this.offset$).pipe(
      debounceTime(200),
      tap(([offset]) => this.router.navigate([], {
        relativeTo: this.route,
        queryParams: {
          page: offset / this.limit + 1
        },
        queryParamsHandling: 'merge'
      })),
      liveSearch(([offset]) =>
        this.collectionService.getCards(this.limit, offset), 0)
    );

    // Устанавливаем значения контролов формы, так они пробросятся в UI.
    // Пользовательского представления у смещения из формы нет.
  }

  onPageChange() {
    const offset = (this.currentPage - 1) * this.limit;
    this.f.offset.setValue(offset);
  }
}
