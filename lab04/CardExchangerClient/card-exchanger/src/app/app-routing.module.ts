import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './_helpers/auth.guard';

import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { CardsComponent } from './cards/cards.component';
import { SetsComponent } from './sets/sets.component';
import { CollectionComponent } from './collection/collection.component';
import { DialoguesComponent } from './dialogues/dialogues.component';
import { DialogueComponent } from './dialogue/dialogue.component';
import { ExchangesComponent } from './exchanges/exchanges.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'cards',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'registration',
    component: RegistrationComponent
  },
  {
    path: 'cards',
    component: CardsComponent
  },
  {
    path: 'sets',
    component: SetsComponent
  },
  {
    path: 'collection',
    component: CollectionComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'dialogues',
    component: DialoguesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'dialogues/:id',
    component: DialogueComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'exchanges',
    component: ExchangesComponent,
    canActivate: [AuthGuard]
  }
  /*{
    path: 'admin',
    component: AdminComponent,
    canActivate: [AuthGuard],
    data: { roles: [User.Role.Admin] }
  },*/
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
