import { Component, OnInit } from '@angular/core';
import { Observable, combineLatest } from 'rxjs';
import { startWith, distinctUntilChanged, tap, debounceTime } from 'rxjs/operators';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { Set } from '../_models/set';
import { SetsService } from '../_services/sets.service';
import { liveSearch } from '../_helpers/live-search.operator';

@Component({
  selector: 'app-sets',
  templateUrl: './sets.component.html',
  styleUrls: ['./sets.component.css']
})
export class SetsComponent implements OnInit {
  filterForm: FormGroup;
  setName$: Observable<string>;
  setCode$: Observable<string>;
  offset$: Observable<number>;
  filteredSets$: Observable<Set[]>;
  get f() { return this.filterForm.controls; }

  currentPage: number;
  limit = 15;

  constructor(
    private formBuilder: FormBuilder,
    private setsService: SetsService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    let initParams = this.route.snapshot.queryParams;

    let page = Math.floor(+initParams['page']);
    this.currentPage = page > 0 ? page : 1;

    this.filterForm = this.formBuilder.group({
      setName: null,
      setCode: null,
      offset: null
    });

    // Чтобы сработал оператор combineLatest() из filteredSets$, все
    // контролы формы должны быть заполнены какими-то значениями. Для
    // этого испоьзуем оператор startWith(). Почему проброс через метод
    // setValue() ниже не работает - хз.
    this.setName$ = this.f.setName.valueChanges.pipe(
      startWith(initParams['setName'] || ''),
      distinctUntilChanged()
    );
    this.setCode$ = this.f.setCode.valueChanges.pipe(
      startWith(initParams['setCode'] || ''),
      distinctUntilChanged()
    );
    this.offset$ = this.f.offset.valueChanges.pipe(
      startWith((this.currentPage - 1) * this.limit),
      distinctUntilChanged()
    );
    this.filteredSets$ = combineLatest(this.setName$, this.setCode$, this.offset$).pipe(
      debounceTime(200),
      tap(([setName, setCode, offset]) => this.router.navigate([], {
        relativeTo: this.route,
        queryParams: {
          setName: setName || null,
          setCode: setCode || null,
          page: offset / this.limit + 1
        },
        queryParamsHandling: 'merge'
      })),
      liveSearch(([setName, setCode, offset]) => this.setsService.getSets(this.limit, offset, setName, setCode, true), 0)
    );

    // Устанавливаем значения контролов формы, так они пробросятся в UI.
    this.f.setName.setValue(initParams['setName']);
    this.f.setCode.setValue(initParams['setCode']);
    // Пользовательского представления у смещения из формы нет.
  }

  onPageChange() {
    const offset = (this.currentPage - 1) * this.limit;
    this.f.offset.setValue(offset);
  }
}
