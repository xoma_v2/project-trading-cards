import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { User } from '../_models/user';

@Injectable({
    providedIn: 'root'
})
export class UsersService {
    constructor(
        private http: HttpClient,
        @Inject('BASE_PATH') private basePath: string
    ) { }

    getUsers(login?: string): Observable<User[]> {
        let params = new HttpParams();

        if (login) params = params.set("login", login);

        return this.http.get<User[]>(this.basePath + "api/users", { params: params });
    }

    getUserById(id: any): Observable<User> {
        return this.http.get<User>(`${this.basePath}api/users/${id}`);
    }
}