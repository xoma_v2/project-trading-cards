import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Card } from '../_models/card';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class CollectionService {
  constructor(
    private http: HttpClient,
    @Inject('BASE_PATH') private basePath: string,
    private authenticationService: AuthenticationService
  ) { }

  getCards(limit?: number, offset?: number): Observable<Card[]> {
    let params = new HttpParams();

    if (limit) params = params.set("limit", limit.toString());
    if (offset) params = params.set("offset", offset.toString());

    return this.http.get<Card[]>(`${this.basePath}api/users/${this.authenticationService.currentUserValue.id}/cards`, { params: params });
  }

  addCard(cardId: number) {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      body: {
        cardId: cardId,
        count: 1,
        userId: this.authenticationService.currentUserValue.id
      }
    };

    return this.http.post<any>(`${this.basePath}api/users/${this.authenticationService.currentUserValue.id}/cards`, {
      cardId: cardId,
      count: 1,
      userId: this.authenticationService.currentUserValue.id
    });
  }

  removeCard(cardId: number) {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      body: {
        cardId: cardId,
        count: 1,
        userId: this.authenticationService.currentUserValue.id
      }
    };

    return this.http.delete<any>(`${this.basePath}api/users/${this.authenticationService.currentUserValue.id}/cards`, options);
  }
}
