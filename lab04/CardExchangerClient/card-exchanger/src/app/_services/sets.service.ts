import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, forkJoin } from 'rxjs';
import { mergeMap, map } from 'rxjs/operators';

import { Set } from '../_models/set';
import { Card } from '../_models/card';

@Injectable({
    providedIn: 'root'
})
export class SetsService {
    constructor(
        private http: HttpClient,
        @Inject('BASE_PATH') private basePath: string
    ) { }

    getSets(limit?: number, offset?: number, setName?: string, setCode?: string, withCards?: boolean): Observable<Set[]> {
        let params = new HttpParams();

        if (limit) params = params.set("limit", limit.toString());
        if (offset) params = params.set("offset", offset.toString());
        if (setName) params = params.set("setName", setName);
        if (setCode) params = params.set("setCode", setCode);

        //console.log(params.toString());

        let query$ = this.http.get<Set[]>(this.basePath + "api/sets", { params: params });

        if (withCards) {
            query$ = query$.pipe(
                mergeMap(sets => forkJoin(
                    sets.map(set => this.getCardsBySetId(set.id)
                        .pipe(map(cards => {
                            set.cards = cards;
                            return set;
                        }))
                    )
                ))
            );
        }

        return query$;
    }

    getSetById(id: any): Observable<Set> {
        return this.http.get<Set>(`${this.basePath}api/sets/${id}`);
    }

    getCardsBySetId(setId: any): Observable<Card[]> {
        return this.http.get<Card[]>(`${this.basePath}api/sets/${setId}/cards`);
    }
}