import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Message } from '../_models/message';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {
  constructor(
    private http: HttpClient,
    @Inject('BASE_PATH') private basePath: string
  ) { }

  getMessages(dialogueId: any): Observable<Message[]> {
    return this.http.get<Message[]>(`${this.basePath}api/dialogues/${dialogueId}/messages`);
  }

  getLastMessage(dialogueId: any): Observable<Message[]> {
    return this.http.get<Message[]>(`${this.basePath}api/dialogues/${dialogueId}/messages/last`);
  }
}
