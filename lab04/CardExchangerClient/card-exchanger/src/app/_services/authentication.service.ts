import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from '../_models/user';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(
        private http: HttpClient,
        @Inject('BASE_PATH') private basePath: string
    ) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(login: string, password: string) {
        let params = new HttpParams()
            .set("login", login)
            .set("password", password);

        return this.http.get<any>(`${this.basePath}api/users/login`, { params: params })
            .pipe(map(user => {
                // Login successful if there's a jwt token in the response.
                if (user && user.token) {
                    // Store user details and jwt token in local storage to keep user logged in between page refreshes.
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    this.currentUserSubject.next(user);
                }

                return user;
            }));
    }

    logout() {
        // Remove user from local storage to log user out.
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
}