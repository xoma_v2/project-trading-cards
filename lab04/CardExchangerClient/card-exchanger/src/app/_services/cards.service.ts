import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Card } from '../_models/card';

@Injectable({
  providedIn: 'root'
})
export class CardsService {
  constructor(
    private http: HttpClient,
    @Inject('BASE_PATH') private basePath: string
  ) { }

  getCards(limit?: number, offset?: number, cardText?: string, setCode?: string, cardType?: string): Observable<Card[]> {
    let params = new HttpParams();

    if (limit) params = params.set("limit", limit.toString());
    if (offset) params = params.set("offset", offset.toString());
    if (cardText) params = params.set("cardText", cardText);
    if (setCode) params = params.set("setCode", setCode);
    if (cardType) params = params.set("cardType", cardType);

    console.log(params.toString());

    return this.http.get<Card[]>(this.basePath + "api/cards", { params: params });
  }

  getCardById(id: any): Observable<Card> {
    return this.http.get<Card>(`${this.basePath}api/cards/${id}`);
  }
}
