import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class RegistrationService {
    constructor(
        private http: HttpClient,
        @Inject('BASE_PATH') private basePath: string
    ) { }

    registrate(login: string, password: string): Observable<any> {
        return this.http.post<any>(`${this.basePath}api/users/singin`, { login, password });
    }
}
