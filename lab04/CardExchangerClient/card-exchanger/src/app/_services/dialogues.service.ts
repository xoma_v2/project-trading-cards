import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, forkJoin } from 'rxjs';
import { mergeMap, map } from 'rxjs/operators';

import { Dialogue } from '../_models/dialogue';
import { AuthenticationService } from './authentication.service';
import { MessagesService } from './messages.service';
import { UsersService } from './users.service';

@Injectable({
    providedIn: 'root'
})
export class DialoguesService {
    constructor(
        private http: HttpClient,
        private authenticationService: AuthenticationService,
        private messagesService: MessagesService,
        private usersService: UsersService,
        @Inject('BASE_PATH') private basePath: string
    ) { }

    getDialogueById(dialogueId: any): Observable<Dialogue> {
        return this.http.get<Dialogue>(`${this.basePath}api/dialogues/${dialogueId}`);
    }

    getDialogues(withLastMessage?: boolean): Observable<Dialogue[]> {
        const currentUser = this.authenticationService.currentUserValue;

        let query$ = this.http.get<Dialogue[]>(`${this.basePath}api/users/${currentUser.id}/dialogues`);

        if (withLastMessage) {
            query$ = query$.pipe(
                mergeMap(dialogues => forkJoin(
                    dialogues.map(dialogue => this.messagesService.getLastMessage(dialogue.id).pipe(
                        map(lastMessage => {
                            dialogue.lastMessage = lastMessage;
                            return dialogue;
                        })
                    ))
                ))
            );
        }

        return query$;
    }

    createDialogue(user1Id: number, user2Id: number): Observable<Dialogue> {
        return this.http.post<Dialogue>(`${this.basePath}api/dialogues`, { user1Id, user2Id });
    }

    /*getDialoguesWithMessages(): Observable<Dialogue[]> {
        const currentUser = this.authenticationService.currentUserValue;

        return this.getDialogues().pipe(
            mergeMap(dialogues => forkJoin(
                dialogues.map(dialogue => this.messagesService.getMessages(dialogue.id).pipe(
                    mergeMap(messages => {
                        dialogue.messages = messages;
                        return this.usersService.getUserById(dialogue.user1Id === currentUser.id
                            ? dialogue.user2Id
                            : dialogue.user1Id).pipe(
                                map(user => {
                                    dialogue.otherUserLogin = user.login;
                                    return dialogue;
                                })
                            );
                    })
                ))
            ))
        );
    }*/
}
