import { Card } from './card';

export interface Set {
    id: number;
    name: string;
    releaseDate: Date;
    code: string;
    cards?: Card[];
}