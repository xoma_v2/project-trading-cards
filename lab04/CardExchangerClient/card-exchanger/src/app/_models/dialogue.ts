import { Message } from './message';

export interface Dialogue {
    id: number;
    user1Id: number;
    user2Id: number;
    creationTime: Date;
    otherUserLogin?: string;
    messages?: Message[];
    lastMessage?: Message[];
}