export interface Card {
    id: number;
    name: string;
    description?: string;
    image: string;
    mana?: string;
    price: number;
    setCode: string;
    types: string[];
    count?: number;
}