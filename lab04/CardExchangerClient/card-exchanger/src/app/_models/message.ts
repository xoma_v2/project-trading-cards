export interface Message {
    id: number;
    dialogueId: number;
    text: string;
    time: Date;
    userId: number;
}