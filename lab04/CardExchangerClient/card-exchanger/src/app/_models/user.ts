export interface User {
    id: number;
    login: string;
    password?: string;
    name?: string;
    role: User.Role;
    token?: string;
}

export namespace User {
    export const enum Role {
        User,
        Admin
    }
}