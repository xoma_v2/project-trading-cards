import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbThemeModule, NbLayoutModule, NbChatModule, NbToastrModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';

import { JwtInterceptor } from './_helpers/jwt.interceptor';
import { ErrorInterceptor } from './_helpers/error.interceptor';
import { fakeBackendProvider } from './_helpers/fake-backend';
import { environment } from 'src/environments/environment';

import { CardsFilterPipe } from './_pipes/cards-filter.pipe';
import { SetsFilterPipe } from './_pipes/sets-filter.pipe';
import { RussianDatePipe } from './_pipes/russian-date.pipe';
import { BoldSpanPipe } from './_pipes/bold-span.pipe';

import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { CardsComponent } from './cards/cards.component';
import { SetsComponent } from './sets/sets.component';
import { CollectionComponent } from './collection/collection.component';
import { DialoguesComponent } from './dialogues/dialogues.component';
import { ExchangesComponent } from './exchanges/exchanges.component';
import { DialogueComponent } from './dialogue/dialogue.component';

@NgModule({
   declarations: [
      AppComponent,
      LoginComponent,
      RegistrationComponent,
      CardsComponent,
      SetsComponent,
      CollectionComponent,
      DialoguesComponent,
      DialogueComponent,
      ExchangesComponent,
      CardsFilterPipe,
      SetsFilterPipe,
      RussianDatePipe,
      BoldSpanPipe
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      NgbModule,
      ReactiveFormsModule,
      HttpClientModule,
      BrowserAnimationsModule,
      NbThemeModule.forRoot({ name: 'default' }),
      NbLayoutModule,
      NbEvaIconsModule,
      NbChatModule,
      NbToastrModule.forRoot(),
   ],
   providers: [
      { provide: 'BASE_PATH', useValue: environment.apiUrl },
      { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
      { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

      // Provider used to create fake backend.
      //fakeBackendProvider
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
