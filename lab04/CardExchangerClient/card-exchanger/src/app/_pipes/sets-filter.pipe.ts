import { Pipe, PipeTransform } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Set } from '../_models/set';
import { RussianDatePipe } from './russian-date.pipe';

@Pipe({
  name: 'setsFilter'
})
export class SetsFilterPipe implements PipeTransform {
  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) { }

  transform(sets: Set[], searchString: string): Set[] {
    if (!sets)
      return [];

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: { searchString: searchString || null },
      queryParamsHandling: 'merge'
    });

    if (!searchString)
      return sets;

    searchString = searchString.toLowerCase();
    const datePipe = new RussianDatePipe();

    return sets.filter((set, index, array) =>
      set.name.toLowerCase().includes(searchString) ||
      datePipe.transform(set.releaseDate).toLowerCase().includes(searchString) ||
      set.code.toLowerCase().includes(searchString));
  }
}
