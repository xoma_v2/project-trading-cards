import { Pipe, PipeTransform } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Card } from '../_models/card';

@Pipe({
  name: 'cardsFilter'
})
export class CardsFilterPipe implements PipeTransform {
  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) { }
  
  transform(cards: Card[], cardText?: string, setCode?: string, mana?: string): Card[] {
    if (!cards)
      return [];

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        cardText: cardText || null,
        setCode: setCode || null,
        mana: mana || null
      },
      queryParamsHandling: 'merge'
    });

    if (cardText) {
      cardText = cardText.toLowerCase();
      cards = cards.filter((card, index, array) =>
        card.name.toLowerCase().includes(cardText) ||
        card.description && card.description.toLowerCase().includes(cardText)
      );
    }

    if (setCode) {
      setCode = setCode.toLowerCase();
      cards = cards.filter((card, index, array) =>
        card.setCode.toLowerCase().includes(setCode)
      );
    }

    if (mana) {
      mana = mana.toLowerCase();
      cards = cards.filter((card, index, array) =>
        card.mana && card.mana.replace(/{|}/gi, '').toLowerCase().includes(mana)
      );
    }

    return cards;
  }
}
