import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ruDate'
})
export class RussianDatePipe implements PipeTransform {
  transform(date: Date): string {
    // Даты из C# формата нужно переформатировать в формат TS.
    date = new Date(`${date}`);

    return `${date.getDate()}.${date.getMonth() + 1}.${date.getFullYear()}`;
  }
}
