import { Component, OnInit } from '@angular/core';
import { Observable, combineLatest } from 'rxjs';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { distinctUntilChanged, startWith, tap, debounceTime } from 'rxjs/operators';

import { CardsService } from '../_services/cards.service';
import { Card } from '../_models/card';
import { liveSearch } from '../_helpers/live-search.operator';
import { AuthenticationService } from '../_services/authentication.service';
import { NbToastrService } from '@nebular/theme';
import { CollectionService } from '../_services/collection.service';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {
  filterForm: FormGroup;
  cardText$: Observable<string>;
  setCode$: Observable<string>;
  cardType$: Observable<string>;
  offset$: Observable<number>;
  filteredCards$: Observable<Card[]>;
  get f() { return this.filterForm.controls; }

  currentPage: number;
  limit = 24;

  collectionMode = false;
  get showButtons(): boolean {
    return this.authenticationService.currentUserValue && this.collectionMode;
  }

  constructor(
    private formBuilder: FormBuilder,
    private cardsService: CardsService,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private toastrService: NbToastrService,
    private collectionService: CollectionService
  ) { }

  ngOnInit() {
    let initParams = this.route.snapshot.queryParams;
    let page = Math.floor(+initParams['page']);
    this.currentPage = page > 0 ? page : 1;
    this.route.queryParams.subscribe(
      params => this.collectionMode = params['collectionMode'] === 'true'
    );

    this.filterForm = this.formBuilder.group({
      cardText: null,
      setCode: null,
      cardType: null,
      offset: null
    });

    // Чтобы сработал оператор combineLatest() из filteredCards$, все
    // контролы формы должны быть заполнены какими-то значениями. Для
    // этого испоьзуем оператор startWith(). Почему проброс через метод
    // setValue() ниже не работает - хз.
    this.cardText$ = this.f.cardText.valueChanges.pipe(
      startWith(initParams['cardText'] || ''),
      distinctUntilChanged()
    );
    this.setCode$ = this.f.setCode.valueChanges.pipe(
      startWith(initParams['setCode'] || ''),
      distinctUntilChanged()
    );
    this.cardType$ = this.f.cardType.valueChanges.pipe(
      startWith(initParams['cardType'] || ''),
      distinctUntilChanged()
    );
    this.offset$ = this.f.offset.valueChanges.pipe(
      startWith((this.currentPage - 1) * this.limit),
      distinctUntilChanged()
    );
    this.filteredCards$ = combineLatest(this.cardText$, this.setCode$, this.cardType$, this.offset$).pipe(
      debounceTime(200),
      tap(([cardText, setCode, cardType, offset]) => this.router.navigate([], {
        relativeTo: this.route,
        queryParams: {
          cardText: cardText || null,
          setCode: setCode || null,
          cardType: cardType || null,
          page: offset / this.limit + 1
        },
        queryParamsHandling: 'merge'
      })),
      liveSearch(([cardText, setCode, cardType, offset]) =>
        this.cardsService.getCards(this.limit, offset, cardText, setCode, cardType), 0)
    );

    // Устанавливаем значения контролов формы, так они пробросятся в UI.
    this.f.cardText.setValue(initParams['cardText']);
    this.f.setCode.setValue(initParams['setCode']);
    this.f.cardType.setValue(initParams['cardType']);
    // Пользовательского представления у смещения из формы нет.
  }

  onPageChange() {
    const offset = (this.currentPage - 1) * this.limit;
    this.f.offset.setValue(offset);
  }

  showToast(title: string, message: string, position, status) {
    this.toastrService.show(
      message, title, { position, status }
    );
  }

  addCard(card: Card) {
    this.collectionService.addCard(card.id).subscribe(
      value => this.showToast('Добавление', `Карта "${card.name}" добавлена в коллекцию!`, 'bottom-right', 'success')
    );
  }

  removeCard(card: Card) {
    this.collectionService.removeCard(card.id).subscribe(
      value => this.showToast('Удаление', `Карта "${card.name}" удалена из коллекции!`, 'bottom-right', 'danger')
    );
  }
}
