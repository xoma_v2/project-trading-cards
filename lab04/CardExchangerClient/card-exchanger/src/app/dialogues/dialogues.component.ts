import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { FormBuilder, FormGroup } from '@angular/forms';

import { UsersService } from '../_services/users.service';
import { User } from '../_models/user';
import { DialoguesService } from '../_services/dialogues.service';
import { Dialogue } from '../_models/dialogue';
import { AuthenticationService } from '../_services/authentication.service';
import { liveSearch } from '../_helpers/live-search.operator';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dialogues',
  templateUrl: './dialogues.component.html',
  styleUrls: ['./dialogues.component.css']
})
export class DialoguesComponent implements OnInit {
  dialogues$: Observable<Dialogue[]>;

  filterForm: FormGroup;
  filteredUsers$: Observable<User[]>;
  get f() { return this.filterForm.controls; }

  //isLoading = false;

  constructor(
    private formBuilder: FormBuilder,
    private dialoguesService: DialoguesService,
    private usersService: UsersService,
    private authenticationService: AuthenticationService,
    private router: Router
  ) { }

  ngOnInit() {
    this.dialogues$ = this.dialoguesService.getDialogues(true);

    this.filterForm = this.formBuilder.group({
      login: null
    });

    this.filteredUsers$ = this.f.login.valueChanges.pipe(
      liveSearch((login: string) => login ? this.usersService.getUsers(login) : of(null))
    );
  }

  createDialogue(userId: number) {
    this.dialoguesService.createDialogue(userId, this.authenticationService.currentUserValue.id)
      .subscribe(
        (dialogue) => this.router.navigate([`/dialogues/${dialogue.id}`])
      );
  }
}
