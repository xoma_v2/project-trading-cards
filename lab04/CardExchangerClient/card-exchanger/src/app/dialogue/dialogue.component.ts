import { Component, OnInit, Inject } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { Observable, forkJoin } from 'rxjs';
import { mergeMap, map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

import { Dialogue } from '../_models/dialogue';
import { Message } from '../_models/message';
import { AuthenticationService } from '../_services/authentication.service';
import { User } from '../_models/user';
import { DialoguesService } from '../_services/dialogues.service';
import { UsersService } from '../_services/users.service';
import { MessagesService } from '../_services/messages.service';

@Component({
  selector: 'app-dialogue',
  templateUrl: './dialogue.component.html'
})
export class DialogueComponent implements OnInit {
  dialogueId: number;
  dialogue$: Observable<Dialogue>;
  otherUser$: Observable<User>;
  otherUserLogin$: Observable<string>;
  currentUser: User;
  messages: any[];
  hubConnection: HubConnection;

  constructor(
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private dialoguesService: DialoguesService,
    private usersService: UsersService,
    private messagesService: MessagesService,
    @Inject('BASE_PATH') private basePath: string
  ) {
    this.messages = [];
  }

  ngOnInit() {
    this.currentUser = this.authenticationService.currentUserValue;
    this.dialogueId = +this.route.snapshot.paramMap.get('id');
    this.dialogue$ = this.dialoguesService.getDialogueById(this.dialogueId);
    this.otherUser$ = this.dialogue$.pipe(
      mergeMap((dialogue) => this.usersService.getUserById(dialogue.user1Id === this.currentUser.id ? dialogue.user2Id : dialogue.user1Id))
    );
    this.otherUserLogin$ = this.otherUser$.pipe(
      map((user) => user.login)
    );
    this.messagesService.getMessages(this.dialogueId).pipe(
      map(messages => {
        let messagesView = [];

        messages.forEach(message => messagesView.push({
          text: message.text,
          date: message.time,
          reply: message.userId === this.currentUser.id,
          type: 'text',
          files: []
        }));

        return messagesView;
      })
    ).subscribe(messages => this.messages = messages);

    this.hubConnection = new HubConnectionBuilder()
      .withUrl(`${this.basePath}chat`, { accessTokenFactory: () => this.authenticationService.currentUserValue.token })
      .build();

    this.hubConnection.on('Recieve', (message: Message) => {
      this.messages.push({
        text: message.text,
        date: message.time,
        reply: message.userId === this.currentUser.id,
        type: 'text',
        files: []
      });
    });

    this.hubConnection.on('onSendError', (message: string) => {
      console.error(message);
    })

    this.hubConnection
      .start()
      .catch(err => console.log('Error while establishing connection :('));
  }

  sendMessage(event: any) {
    this.hubConnection
      .invoke('Send', { userId: this.currentUser.id, dialogueId: this.dialogueId, text: event.message })
      .catch(err => console.error(err));
  }
}
