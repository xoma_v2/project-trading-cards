## Игра magic: the gathering

**Magic: The Gathering (MTG, Magic, Elks)** — настольная игра, выпускаемая компанией «Wizards of the Coast». Относится к классам коллекционных и логических карточных игр.  Каждый год появляется 600-700 новых карт. Один из способов получения нужных карт — обмен с другими игроками.

#### Описание ролей пользователя

![User roles](https://gitlab.com/xoma_v2/project-trading-cards/-/raw/master/lab03/png/UserRoles.png)

#### Сущности предметной области

![DB](https://gitlab.com/xoma_v2/project-trading-cards/-/raw/master/lab03/png/DB.png)

#### Прототип интерфейса

###### страница пользователя:

![User page](https://gitlab.com/xoma_v2/project-trading-cards/-/raw/master/lab03/png/UserPage.png)

На данной странице можно просмотреть свою коллекцию карт и добавить новые.

###### чаты:

![Dialogues](https://gitlab.com/xoma_v2/project-trading-cards/-/raw/master/lab03/png/Dialogues.png)

На данной странице выбирается чат или начинается новый.

###### сообщения:

![Dialogue](https://gitlab.com/xoma_v2/project-trading-cards/-/raw/master/lab03/png/Dialogue.png)

На данной странице происходит диалог с собеседником и присутствует возможность просмотреть предложения обмена.

###### обмен:

![Exchange](https://gitlab.com/xoma_v2/project-trading-cards/-/raw/master/lab03/png/Exchange.png)

На данной странице можно просмотреть предложение обмена, добавить карты к предложению и объявить готовность к обмену.

Для подтверждения совершения физического обмена пользователи оба участника должны зайти на соответствующий раздел сайта и нажать кнопку "Подтвердить физический обмен":
![Exchanges](https://gitlab.com/xoma_v2/project-trading-cards/-/raw/master/lab03/png/Exchanges.png)

#### Архитектура приложения

Так как при осуществлении обмена сообщениями между пользователями а также при выполнении обмена коллекциями карт требуется отображение изменений в реальном времени, эти операции будут осуществляться с использованием протокола WebSocket.  Обмен прочими данными (списки карт, пользовательские коллекции и прочие запросы из базы данных) будет происходить по протоколу HTTP.

###### RESTful API:

Описание | Запрос | URI
-- | -- | --
Создать новый аутентификационный токен пользователя по логину и паролю: | POST | /accounts?login=my_login&password=my_password
-- | -- | --
Вернуть список карт: | GET | /cards
Вернуть список карт с пагинацией: | GET | /cards?offset=0&limit=15
Вернуть данные о карте по её id: | GET | /cards/{card_id} 
Вернуть список карт по коду сета, в котором они были выпущены: | GET | /cards?setcode={set_code}
Вернуть список карт по id сета, в котором они были выпущены: | GET | /cards?setid={set_id}
Обновить данные карты по её id: | PUT | /cards/{card_id} 
Создать новую карту в разделе cards: | POST | /cards
Удалить карту по её id: | DELETE | /cards/{card_id} 
-- | -- | --
Вернуть список сетов: | GET | /sets
-- | -- | --
Вернуть список пользователей: | GET | /users
Вернуть список пользователей с пагинацией: | GET | /users?offset=0&limit=15 
Вернуть данные о пользователе по его id: | GET | /users/{user_id} 
Вернуть список карт пользователя по его id: | GET | /users/{user_id}/cards
Вернуть данные о карте с {card_id} пользователя с {user_id}: | GET | /users/{user_id}/cards/{card_id} 
Обновить данные пользователя по его id: | PUT | /users/{user_id} 
Создать нового пользователя в разделе users: | POST | /users
Добавить пользователю с {user_id} карту с {card_id}: | POST | /users/{user_id}/cards/{card_id} 
Удалить у пользователя с {user_id} карту с {card_id}: | DELETE | /users/{user_id}/cards/{card_id} 
Удалить пользователя по его id: | DELETE | /users/{user_id} 
-- | -- | --
Вернуть список чатов с участием пользователя с {user_id}: | GET | /chats?member={user_id}
Вернуть список сообщений по id чата: | GET | /chats/{chat_id}
Вернуть список сообщений по id чата с пагинацией: | GET | /chats/{chat_id}?offset=0&limit=15
Обновить сообщение с {message_id} в чате с {chat_id}: | PUT | /chats/{chat_id}/{message_id}
Удалить чат по его id: | DELETE | /chats/{chat_id} 
Удалить сообщение по его {message_id} в чате с {chat_id}: | DELETE | /chats/{chat_id}/{message_id} 

Обмен сообщениями в чате с {chat_id} по протоколу WebSockets:
ws://localhost:80/api/chats/{chat_id} 


#### Технические детали реализации

Backend стек: ASP.NET Core, MS SQL Server.

Fronend стек: Angular, HTML, CSS.

###### Команда:

- Карпухин Александр ( @kas16u376 ) - teamlead, developer
- Магазинов Николай ( @mna16u149 ) - developer, tester
- Иксарица Никита ( @ini16u374 ) - developer

#### Структура и подход к CI/CD для проекта

Предполагается реализовать три стадии:

- build
- test
- release

На стадии build будет осуществляться компиляция бэкэнда и фронтенда. Стадия test - прогон тестов. Работы стадии release будут выполняться только для ветки master. На этой стадии будет осуществляться сборка docker-образа для бэкенда и архивация файлов фронтэнда.

